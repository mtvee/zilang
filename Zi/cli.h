///-------------------------------------------------------------------------------------------------
// file:	Zi\Zi\cli.h
//
// summary:	Declares the CLI namespace

#pragma once

#include <string>
#include <vector>
#include <map>
#include <utility>
#include <iostream>
#include <memory>


///-------------------------------------------------------------------------------------------------
// namespace: cli
//
// summary:	the cli namespace.
namespace cli
{
	typedef std::vector<std::string> strlist;

	// not really used other than to indicate if an option
	// is a flag or not
	enum class OptType { String, Flag };

	///-------------------------------------------------------------------------------------------------
	/// <summary>	hold our option info the validity checking and parsing. </summary>
	///
	struct Opt
	{
		Opt(std::string n, std::string desc, OptType t)
			: name(n), description(desc), type(t), value("") {}
		std::string name;
		std::string value;
		std::string description;
		OptType     type;
	};

	typedef std::pair<std::string, std::shared_ptr<Opt>>     optitem;
	typedef std::multimap<std::string, std::shared_ptr<Opt>> optmap;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	A simple option parser for the command line. </summary>
	///
	/// <example>
	/// 	$progname -mcl ---bar foo -fud=bleh fizz buzz
	/// 	
	/// 	cli::Options opt;
	/// 	opt.addOpt( "mcl", cli::OptType::Flag);
	/// 	opt.addOpt( "bar", cli::OptType::String);
	/// 	opt.addOpt( "fud", cli::OptType::String);
	/// 	opt.parse(argc, argv);
	/// 	bool script_console = opt.hasFlag("mcl");// true std::string val = opt.getValue("bar");
	/// 	// "foo" cli::strlist vals = opt.getValues("bar");// {"foo"}
	/// 	std::string val2 = opt.getValue("fud");  // "bleh" cli::strlist rest = opt.getRest();
	/// 	// {"fizz","buzz"}
	/// </example>
	class Options
	{
	public:
		Options() {};
		~Options() {
			opts.clear();
		}

		std::string progname() { return _progname; }
		std::string progpath() { return _progpath; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Add a legal option. </summary>
		///
		/// <param name="name">	optiont name. </param>
		/// <param name="typ"> 	OptType. </param>
		/// <param name="desc">	The description. </param>
		void addOpt(std::string name, OptType typ, std::string desc = "")
		{
			Opt o(name, desc, typ);
			optitem oi = { name, std::make_shared<Opt>(o) };
			opts.insert(oi);
		}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Parse a strlist. </summary>
		///
		/// <param name="_args">	the list. </param>
		void parse(strlist _args) 
		{
			args = _args;
			parse();
		}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Parse an array of count. </summary>
		///
		/// <param name="argc">	array count. </param>
		/// <param name="argv">	[in,out] array. </param>
		void parse(int argc, char **argv) 
		{
			std::string exepath(argv[0]);
#ifdef _WIN32
			size_t pos = exepath.rfind('\\');
#else
			size_t pos = exepath.rfind('/');
#endif
			if (pos != std::string::npos) {
				_progpath = exepath.substr(0, pos);
				_progname = exepath.substr(pos + 1);
			}
			else {
				_progname = argv[0];
			}

			for (int i = 1; i < argc; i++) {
				std::string arg = argv[i];
				if (arg[0] == '-') {
                    // eat all the -
					while (arg[0] == '-') 
                        arg = arg.substr(1);
					arg = '-' + arg;
				}
				args.push_back(arg);
			}
			parse();
		}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Return the first value of opt. </summary>
		///
		/// <param name="opt">	the name of the option. </param>
		///
		/// <returns>	the value or an empty string. </returns>
		std::string getValue(const std::string opt) 
		{
			for (auto o : opts) {
				if (o.first == opt) {
					return o.second->value;
				}
			}
			return "";
		}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Return a strlist of values. </summary>
		///
		/// <param name="opt">	the option name. </param>
		///
		/// <returns>	a strlist. </returns>
		strlist getValues(const std::string opt) 
		{
			strlist ret;
			for (auto o : opts) {
				if (o.first == opt) {
					ret.push_back(o.second->value);
				}
			}
			return ret;
		}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Return the rest of the unassociated arguments like filenames. </summary>
		///
		/// <returns>	strlist. </returns>
		strlist getRest() 
		{
			return rest;
		}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Return if an option was found. </summary>
		///
		/// <exception cref="std::runtime_error">	Raised when a runtime error condition occurs. </exception>
		///
		/// <param name="opt">	the option name. </param>
		///
		/// <returns>	bool. </returns>
		bool hasFlag(const std::string opt) 
		{
			if (opts.count(opt) == 0)
				throw std::runtime_error("Invalid option: " + opt);
			auto fnd = opts.find(opt);
			return ((*fnd).second->value == "true");
		}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Print a usuage screen to cout. </summary>
		///
		void usage() 
		{
			std::cout << "Usage: " << _progname << " [OPTIONS] [ARGS]" << std::endl;
			std::cout << "Where OPTIONS are:" << std::endl;
			for (auto opt : opts) {
				std::cout << "\t-" << opt.first << "\t" << opt.second->description << std::endl;
			}
			std::cout << std::endl;
			std::cout.flush();
		}

	protected:

		///-------------------------------------------------------------------------------------------------
		/// <summary>
		/// 	Does the actual parsing and validity check. Can throw a std::runtime_error for an illegal
		/// 	option.
		/// </summary>
		///
		/// <exception cref="std::runtime_error">	Raised when a runtime error condition occurs. </exception>
		void parse() 
		{
			for (unsigned int i = 0; i < args.size(); i++) {
				if (args[i][0] == '-') {
					// check for an equal sign
					std::string arg(args[i]);
					std::size_t pos = arg.find('=');
					std::string opt;
					if (pos != std::string::npos) {
						opt = arg.substr(0, pos);
					}
					else {
						opt = args[i];
					}
					auto fnd = opts.find(opt.substr(1));
					if (fnd != opts.end()) {
						if ((*fnd).second->type != OptType::Flag) {
							// if there is an equal sign, split off that for the value
							if (pos != std::string::npos) {
								(*fnd).second->value = arg.substr(pos + 1);
							}
							// we expect the next arg to be the value
							else {
								// make sure we have enough args
								if (i + 1 < args.size()) {
									// make sure it's not another option
									if (args[i + 1][0] != '-') {
										(*fnd).second->value = args[i + 1];
										i++;
									}
									else {
										throw std::runtime_error("Expected arg for: " + args[i]);
									}
								}
								else {
									throw std::runtime_error("Expected arg for: " + args[i]);
								}
							}
						}
						else {
							(*fnd).second->value = "true";
						}
					}
					else {
						throw std::runtime_error("Unknown option: " + args[i]);
					}
				}
				else {
					// push the rest of the arguments
					rest.push_back(args[i]);
				}
			}
		}

	private:
		// the complate list of arguments
		strlist							args;
		// the unassociated arguments
		strlist                         rest;
		// a map of option name and Option struct
		// to check against for validity
		optmap							opts;
		// program name
		std::string						 _progname;
		// program path
		std::string						 _progpath;
	};

} // namespace jutils


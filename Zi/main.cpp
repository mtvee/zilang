/**
  Zi language script runner or repl
*/
#include <iostream>
#include "cli.h"
#include "zi.h"

// this windows junk is to test memory leakage in debug mode
// it prints a report of unallocated memory on program termination
#if defined(_WIN32) && defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif  // _WIN32 && _DEBUG

///-------------------------------------------------------------------------------------------------
/// <summary>	Main entry-point for this application. </summary>
///
/// <param name="argc">	Number of command-line arguments. </param>
/// <param name="argv">	Array of command-line argument strings. </param>
///
/// <returns>	Exit-code for the process - 0 for success, else an error code. </returns>
int main(int argc, char **argv)
{
#if defined(_WIN32) && defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	
	// this for specifying an allocation number from the report to
	// break on in visual studio.
	//_CrtSetBreakAlloc(7425);
#endif // _WIN32 && _DEBUG

	cli::Options opt;
	opt.addOpt( "version", cli::OptType::Flag,   "print version and exit");
	opt.addOpt( "help",    cli::OptType::Flag,   "print this screen");
	opt.addOpt( "init",    cli::OptType::String, "load specified file as init");

    try {
        opt.parse(argc, argv);
    } catch( std::exception &e) {
        std::cerr << "option error: " << e.what() << std::endl;
        return 255;
    }
    
	if (opt.hasFlag("help")) {
		opt.usage();
		return 0;
	}
	if (opt.hasFlag("version")) {
		zi::print_logo();
		return 0;
	}

	// initialize the engine
	zi::init();
	
	// set up some default location to search for the rc
	zi::strlist rcpaths;
	if (opt.getValue("init").empty()) {
		rcpaths.push_back(zi::homeLocation() + "/.config/zi/zirc");
		rcpaths.push_back(zi::homeLocation() + "/.zi/zirc");
		rcpaths.push_back(zi::homeLocation() + "/.zirc");
		rcpaths.push_back(opt.progpath() + "/zirc");
		rcpaths.push_back("./zirc");
	} else {
        rcpaths.push_back(opt.getValue("init"));
	}
	
	// try and load the rc
	if (!zi::loadrc(rcpaths))
		std::cerr << "** 'zirc' init file not found" << std::endl;

	int exit_code = 0;
	// check for files on the command line
	if (opt.getRest().size() > 0) {
		for (auto fname : opt.getRest()) {
			try {
				std::string code;
				zi::slurp(fname, code);
				zi::eval_string(code);
			}
			catch (std::exception e) {
				std::cerr << e.what() << std::endl;
				zi::cleanup(); // see below
				return 1;
			}
		}
	}
	// no files, so repl
	else {
		zi::print_logo();
		exit_code = zi::repl();
	}

	// this strictly isn't necessary if we terminate properly
	// but it is useful for putting away the engine when
	// embedded.
	zi::cleanup();
	return exit_code;
}

" Vim syntax file
" Language:	zI language
" Maintainer:  	J. Knight http://github.com/mtvee
" Last Change:	Wed Sep 18 19:08:40 CEST 2002

" The zI Home Page is located at
" http://github.com/mtvee/zi

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn case ignore

syn match ziNoSepError "\(\"\|\k\)\+" contained

" integer
syn match   ziNumber	"[+-]\=[0-9]\d*L\=" nextgroup=ziNoSepError

" floats
syn match ziFloat "[+-]\=\d\+[Ee][+-]\=\d\+" nextgroup=ziNoSepError
syn match ziFloat "[+-]\=\.\d\+\([Ee][+-]\=\d\+\)\=" nextgroup=ziSepError
syn match nListFloat "[+-]\=\d\+\.\d*\([Ee][+-]\=\d\+\)\=" nextgroup=ziSepError

" strings
syn region ziString start=/\v"/ skip=/\v\\./ end=/\v"/ nextgroup=ziNoSepError


"zi functions
syn keyword ziFunction	cons context def defmacro dump exists\? set if fn null? chr string substring int double list type
syn keyword ziFunction	while cond and not quote begin eval expt modulo sqrt inc dec floor ceil ln log10 rand
syn keyword ziFunction	sin cos tan asin acos atan max min print println append apply fold map filter push-back! pop-back!
syn keyword ziFunction       nth length read-string load read-file write-file exit system getenv
syn keyword ziFunction       open close let
" zi variables
syn keyword ziVariable	true false nlisp.version nlisp.build E PI

"zi comments
syn match   ziComment	";.*$"
syn match   ziComment        "\%^#!.*"

"zi delimiters
syn match   ziDelim		","

"catch errors caused by wrong parenthesis and brackets
syn cluster ziAll contains=ziRegion,ziDelim,ziFunction,ziVariable,ziComment,ziParenError,ziSpecial,ziNumber,ziClause,ziCommand,ziFloat,ziString
syn region  ziRegion	matchgroup=Delimiter start="(" skip="|.\{-}|" matchgroup=Delimiter end=")" contains=@ziAll
syn match   ziParenError	")"

"zi specials
syn match   ziSpecial	"+"
syn match   ziSpecial	"-"
syn match   ziSpecial	"*"
syn match   ziSpecial	"/"
syn match   ziSpecial	"<"
syn match   ziSpecial	">"
syn match   ziSpecial	"="
syn match   ziSpecial	"&"
syn match   ziSpecial	"%"


" synchronization
syn sync lines=100

" Define the default highlighting.
if version >= 508 || !exists("did_zil_syntax_init")
  if version < 508
    let did_zil_syntax_init = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif
  HiLink ziComment		Comment
  HiLink ziFunction		Function
  HiLink ziClause		Operator
  HiLink ziDelim		Delimiter
  HiLink ziNumber		Number
  HiLink ziFloat		Number
  HiLink ziSpecial		Type
  HiLink ziVariable		Identifier
  HiLink ziString            String
  HiLink ziParenError	Error
  HiLink ziBracketError	Error
  delcommand HiLink
endif

let b:current_syntax = "zil"

" vim: ts=8 nowrap


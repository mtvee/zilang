" Name:     Some zI language stuff
" Author:   J Knight <emptyvee@gmail.com>
" URL:      http://github.com/mtvee/nlisp
" License:  OSI approved MIT licence
" Created:  On a boat on a river
" Modified: 2015 Jun 24
" Version:  0.1

if exists('g:loaded_zil') || &cp
  finish
endif
let g:loaded_zil = 1

" path to the interpreter
let g:nlisp_path = 'zi'

" set lisp indenting
"setlocal indentexpr=
"setlocal lisp
"let b:undo_indent .= '| setlocal lisp<'

" {{{ commands
" this leaves wierdo buffers everywhere
":command! R let f=expand("%")|vnew|execute '.!' . g:nlisp_path . ' "' . f . '"'


" }}}
" {{{ functions

" }}}
" {{{ licence
" ---------------------------------------------------------------------
"
" Copyright (c) 2014 J Knight
"
" Permission is hereby granted, free of charge, to any person obtaining a copy
" of this software and associated documentation files (the "Software"), to deal
" in the Software without restriction, including without limitation the rights
" to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
" copies of the Software, and to permit persons to whom the Software is
" furnished to do so, subject to the following conditions:
"
" The above copyright notice and this permission notice shall be included in
" all copies or substantial portions of the Software.
"
" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
" IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
" FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
" AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
" LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
" OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
" THE SOFTWARE.
"
" }}}


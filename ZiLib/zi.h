﻿/* <summary>
 Zi, a simple lisp-like scripting language

 much of this is built on code/ideas found in 
 https://bitbucket.org/ktg/paren
 by: (C) 2013-2014 Kim, Taegyoon
*/

/*
Copyright 2015-2016 J. Knight @mtvee

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http ://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.

See the License for the specific language governing permissions and
limitations under the License.
*/

/*
git remote add origin git@github.com:mtvee/zi.git
git push -u origin master

--> or ...origin https://github.com/mtvee/zi
*/

#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <map>
#include <ctime>
#include <memory>
#include <stdexcept>
#include <functional>

#define ZI_VERSION "0.1.0"
// with: git log -1 --pretty="format:%h %ci"
#define ZI_BUILD "67261fd 2016-04-09 13:07:06 -0400"
#define ZI_COPYRIGHT "(c)2015-2016 J. Knight"


///-------------------------------------------------------------------------------------------------
// namespace: zi
//
// summary:	zi namespace.
namespace zi
{
	// custom exception
	// TODO keep the code offset in here
	class zi_exception : public std::runtime_error
	{
	public:
		zi_exception(const std::string msg)
			: std::runtime_error(msg) {}
	};
	// use this instead of std::exit
	class zi_exit_exception : public std::runtime_error
	{
	public:
		zi_exit_exception(int code, const std::string msg = "")
			: std::runtime_error(msg), exit_code(code) {}
		int code() { return exit_code; }
	protected:
		int exit_code;
	};


	// forward decl
	struct  node;
	struct  environment;
	typedef std::shared_ptr<node> snode;
	typedef std::shared_ptr<environment> senvironment;
	typedef snode(*builtin)(std::vector<snode> &args, senvironment &env);

	typedef std::vector<snode>		 snodelist;
	typedef std::vector<std::string> strlist;
	typedef std::map<std::string, snodelist > macromap;
	typedef std::map<std::string, senvironment> ctxmap;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	A node. </summary>
	///
	struct node {
		enum { T_NIL, T_CHAR, T_INT, T_DOUBLE, T_BOOL, T_STRING, 
			   T_SYMBOL, T_LIST, T_SPECIAL, T_BUILTIN, T_RAWPTR, 
			   T_FN, T_OSTREAM, T_ISTREAM, T_NODE_MAX } type;
		union {
			int		v_int, code; // if T_SYMBOL, code for faster access			
			double	v_double;
			bool	v_bool;
			builtin v_builtin;
			void   *v_rawptr;
		};
		// we can keep track of v_rawptr types manually with this
		// so we can stick c++ object into the env and get them
		// back out again
		unsigned int  v_ptrtype;  
		std::string	  v_string;
		snodelist     v_list;
		senvironment  outer_env; // if T_FN

		// node ctors
		node();
		node(char a);
		node(int a);
		node(double a);
		node(bool a);
		node(const std::string &a);
		node(const snodelist &a);
		node(builtin a);
		node(std::ostream *o);
		node(std::istream *i);
		node(void *ptr, unsigned int ptrtype = 0);

		// convert to int
		int    to_int();
		// convert to double
		double to_double();
		// convert to string
		std::string to_string();
		std::string type_str();
		std::string str_with_type();
		unsigned int ptrtype() { return v_ptrtype; }
	};

	extern snode nil;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	An environment. </summary>
	///
	struct environment {
		std::map<int, snode>  env;
		senvironment outer;
		environment(senvironment outer = nullptr);
		snode get(int code);
		snode get(snode &k);
		snode set(snode &k, snode &v);
	};

	// initialize the builtins
	void init();
	void cleanup();
	bool loadrc(strlist paths);

	snode			eval(snode &n, senvironment &env);
	snode			eval_all(snodelist &lst);
	snode			compile(snode &n);
	snodelist		compile_all(snodelist &lst);
	snode			apply(snode &func, snodelist &args, senvironment &env);
	void			print_logo();
	void			prompt();
	void			prompt2();
	snode			eval_string(std::string &s);
	snode			eval_string(const char* s);
	inline void		eval_print(std::string &s);
	int				repl(); // read-eval-print loop

	snode	get(const char *name);
	void	set(const char *name, node value);
	// if this is a rawptr we can keep track of the ptrtype to check for errors
	// when casting back
	void	set(const char *name, node value, unsigned int ptrtype);

	// chaos
	inline double rand_double();

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Print map keys. </summary>
	///
	/// <typeparam name="T">	Generic type parameter. </typeparam>
	/// <param name="m">	[in,out] The std::map&lt;std::string,T&gt; to process. </param>
	template <typename T>
	void print_map_keys(std::map<std::string, T> &m)
	{
		int i = 0;
		for (typename std::map<std::string, T>::iterator iter = m.begin(); iter != m.end(); iter++) {
			std::cout << " " << iter->first;
			i++;
			if (i % 10 == 0) std::cout << std::endl;
		}
		std::cout << std::endl;
	}


	bool	slurp(std::string filename, std::string &str);
	int		spit(std::string filename, const std::string &str);
	int		to_code(const std::string &name);
	snode	make_snode(const node& n);

	strlist		tokenize(const std::string &s);
	snodelist	parse(const std::string &s);

	void expect_atleast(std::string func, int count, snodelist &args);
	void expect_arg(std::string func, int count, snodelist &args);

	extern ctxmap contexts;
	extern std::string currContext;
	extern strlist symname;
	extern macromap macros;
	extern snode node_false;
	extern snode node_true;

	// other modules
#ifndef ZI_EMBEDDED
	void init_os();
#endif

	// os poop
	std::string homeLocation();

} // namespace zi


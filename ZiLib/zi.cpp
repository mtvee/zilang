// zi

#include <ctime>
#include <algorithm>
#include <sstream>

#include "zi.h"

#ifdef _MSC_VER
// stop VC++ from whining about secure CRT stuff
#pragma warning(disable : 4996)
#endif

namespace zi
{
	// contexts
	ctxmap contexts;
	std::string currContext;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Makes a shared node. </summary>
	///
	/// <param name="n">	The node to process. </param>
	///
	/// <returns>	A snode. </returns>
	snode make_snode(const node& n)
	{
		return std::make_shared<node>(n);
	}

	node::node() : type(T_NIL) {}
	node::node(char a) : type(T_CHAR), v_int(a) {}
	node::node(int a) : type(T_INT), v_int(a) {}
	node::node(double a) : type(T_DOUBLE), v_double(a) {}
	node::node(bool a) : type(T_BOOL), v_bool(a) {}
	node::node(const std::string &a) : type(T_STRING), v_string(a) {}
	node::node(const snodelist &a) : type(T_LIST), v_list(a) {}
	node::node(builtin a) : type(T_BUILTIN), v_builtin(a) {}
	node::node(std::ostream *o) : type(T_OSTREAM), v_rawptr(o) {}
	node::node(std::istream *i) : type(T_ISTREAM), v_rawptr(i) {}
	node::node(void *ptr, unsigned int ptrtype) : type(T_RAWPTR), v_rawptr(ptr), v_ptrtype(ptrtype) {}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Makes a special. </summary>
	///
	/// <param name="a">	The builtin to process. </param>
	///
	/// <returns>	A snode. </returns>
	snode make_special(builtin a)
	{
		node n;
		n.type = node::T_SPECIAL;
		n.v_builtin = a;
		return make_snode(n);
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Makes a symbol. </summary>
	///
	/// <param name="name">	The name. </param>
	///
	/// <returns>	A snode. </returns>
	snode make_symbol(std::string name) {
		node n;
		n.type = node::T_SYMBOL;
		n.v_string = name;
		n.code = to_code(name);
		return make_snode(n);
	}

	snode node_true(make_snode(node(true)));
	snode node_false(make_snode(node(false)));
	snode node_0(make_snode(node(0)));
	snode node_1(make_snode(node(1)));
	snode nil(make_snode(node()));

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Converts this object to an int. </summary>
	///
	/// <returns>	This object as an int. </returns>
	inline int node::to_int()
	{
		switch (type) {
		case T_CHAR:
		case T_INT:
			return v_int;
		case T_DOUBLE:
			return (int)v_double;
		case T_BOOL:
			return (int)v_bool;
		case T_STRING:
			return atoi(v_string.c_str());
		default:
			return 0;
		}
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Converts this object to a double. </summary>
	///
	/// <returns>	This object as a double. </returns>
	inline double node::to_double()
	{
		switch (type) {
		case T_INT:
			return v_int;
		case T_DOUBLE:
			return v_double;
		case T_BOOL:
			return v_bool;
		case T_STRING:
			return atof(v_string.c_str());
		default:
			return 0.0;
		}
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Converts this object to a string. </summary>
	///
	/// <returns>	This object as a std::string. </returns>
	inline std::string node::to_string()
	{
		std::stringstream ret;
		switch (type) {
		case T_NIL: 
			ret << "()"; break;
		case T_CHAR:
			ret << "#\\" << (char)v_int; break;
		case T_INT:
			 ret << v_int; break;
		case T_BUILTIN:
		case T_SPECIAL:
			//ret << "#<builtin:" << static_cast<void*>(v_builtin) << ">";
			ret << "#<builtin:" << std::internal << v_builtin << ">";
			break;
		case T_RAWPTR:
			ret << "#<rawptr:" << static_cast<void*>(v_rawptr) << ">";
			break;
		case T_OSTREAM:
			ret << "#<ostream:" << static_cast<void*>(v_rawptr) << ">";
			break;
		case T_ISTREAM:
			ret << "#<istream:" << static_cast<void*>(v_rawptr) << ">";
			break;
		case T_DOUBLE:
			ret << v_double;
			break;
		case T_BOOL:
			ret << (v_bool ? "true" : "false");
			break;
		case T_STRING:
		case T_SYMBOL:
			ret << v_string;
			break;
		case T_FN:
		case T_LIST:
		{
			ret << '(';
			for (snodelist::iterator iter = v_list.begin(); iter != v_list.end(); iter++) {
				if (iter != v_list.begin()) ret << ' ';
				ret << (*iter)->to_string();
			}
			ret << ')';
			break;
		}
		default:;
		}
		return ret.str();
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Type string. </summary>
	///
	/// <returns>	A std::string. of the obj type </returns>
	inline std::string node::type_str()
	{
		switch (type) {
		case T_NIL:
			return "nil";
		case T_CHAR:
			return "char";
		case T_INT:
			return "int";
		case T_DOUBLE:
			return "double";
		case T_BOOL:
			return "bool";
		case T_STRING:
			return "string";
		case T_SYMBOL:
			return "symbol";
		case T_LIST:
			return "list";
		case T_BUILTIN:
			return "builtin";
		case T_RAWPTR:
			return "rawptr";
		case T_OSTREAM:
			return "ostream";
		case T_ISTREAM:
			return "istream";
		case T_SPECIAL:
			return "special";
		case T_FN:
			return "fn";
		default:
			return "unknown type";
		}
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	With type. </summary>
	///
	/// <returns>	A std::string of the object representation with type info</returns>
	inline std::string node::str_with_type()
	{
		return to_string() + " : " + type_str();
	}

	// ====================================================================
	// T O K E N I Z E R 
	// ====================================================================

	/// <summary>	A tokenizer. </summary>
	class tokenizer
	{
	private:
		strlist ret;
		std::string acc; // accumulator
		std::string s;
		void emit() { // add accumulated string to token list
			if (acc.length() > 0) { ret.push_back(acc); acc = ""; }
		}
	public:
		int unclosed; // number of unclosed parenthesis ( or quotation "
		tokenizer(const std::string &s) : s(s), unclosed(0) {}

		strlist tokenize() {
			int last = s.length() - 1;
			for (int pos = 0; pos <= last; pos++) {
				char c = s.at(pos);
				if (c == ' ' || c == '\t' || c == '\r' || c == '\n') {
					emit();
				}
				// end-of-line comment: ; or #!
				else if (c == ';' || (pos < last && c == '#' && s[pos + 1] == '!')) {
					emit();
					do pos++; while (pos <= last && s.at(pos) != '\n');
				}
				// beginning of string
				else if (c == '"') {
					unclosed++;
					emit();
					acc += '"';
					pos++;
					while (pos <= last) {
						if (s.at(pos) == '"') { unclosed--; break; }
						if (s.at(pos) == '\\') { // escape
							char next = s.at(pos + 1);
							if (next == 'r') next = '\r';
							else if (next == 'n') next = '\n';
							else if (next == 't') next = '\t';
							acc += next;
							pos += 2;
						}
						else {
							acc += s.at(pos);
							pos++;
						}
					}
					emit();
				}
				else if (c == '(') {
					unclosed++;
					emit();
					acc += c;
					emit();
				}
				else if (c == ')') {
					unclosed--;
					emit();
					acc += c;
					emit();
				}
				else {
					acc += c;
				}
			}
			emit();
			return ret;
		}
	}; // class tokenizer

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Tokenizes the given s. </summary>
	///
	/// <param name="s">	The std::string to process. </param>
	///
	/// <returns>	A strlist. </returns>
	strlist tokenize(const std::string &s)
	{
		return tokenizer(s).tokenize();
	}

	std::map<std::string, int> symcode;
	strlist symname;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Converts a name to a code. </summary>
	///
	/// <param name="name">	The name. </param>
	///
	/// <returns>	name as an int. </returns>
	int to_code(const std::string &name)
	{
		int r;
		std::map<std::string, int>::iterator found = zi::symcode.find(name);
		if (found != symcode.end()) {
			return found->second;
		}
		else {
			r = symcode.size();
			symcode[name] = r;
			symname.push_back(name);
			return r;
		}
	}

	snode special_quote(snodelist &raw_args, senvironment &env);

	// ====================================================================
	// P A R S E R 
	// ====================================================================

	/// <summary>	A parser. </summary>
	class parser
	{
	private:
		int		pos;
		strlist tokens;

	public:
		parser(const strlist &tokens) : pos(0), tokens(tokens) {}
		snodelist parse() {
			snodelist ret;
			int last = tokens.size() - 1;
			for (; pos <= last; pos++) {
				std::string tok = tokens.at(pos);
				if (tok[0] < 0) break; // MSVC bug fix
				if (tok.at(0) == '"') { // double-quoted string
					ret.push_back(make_snode(tok.substr(1)));
				}
				else if (tok[0] == '#' && tok[1] == '\\') { // char
					ret.push_back(make_snode((char)tok[2]));
				}
				else if (tok[0] == '\'') { // quote					
					snodelist q;
					snode qf = make_symbol("quote");
					q.push_back(qf);
					if (tok.length() > 1 && tok[1] != '(') {
						q.push_back(make_symbol(tok.substr(1)));
						ret.push_back(make_snode(q));
					}
					else {
						pos++;
						int bcount = 0;
						strlist qtokens;
						for (; pos <= last; pos++) {
							if (tokens[pos] == "(") {
								qtokens.push_back(tokens[pos]);
								bcount++;
							}
							else if (tokens[pos] == ")") {
								qtokens.push_back(tokens[pos]);
								bcount--;
							}							
							else if(bcount) {
								if( tokens.size() > 1 )
									qtokens.push_back(tokens[pos]);
							}
							if( !bcount )
								break;
						}
						parser qparser(qtokens);
						snodelist pret = qparser.parse();
						q.push_back(pret.at(0));
						ret.push_back(make_snode(q));
						pos++;
						break;
					}
				}
				else if (tok == "(") { // list
					pos++;
					ret.push_back(make_snode(parse()));
				}
				else if (tok == ")") { // end of list
					break;
				}
				else if (isdigit(tok.at(0)) || (tok.at(0) == '-' && tok.length() >= 2 && isdigit(tok.at(1)))) { // number
					if (tok.find('.') != std::string::npos || tok.find('e') != std::string::npos) { // double
						ret.push_back(make_snode(atof(tok.c_str())));
					}
					else {
						ret.push_back(make_snode(atoi(tok.c_str())));
					}
				}
				else { // symbol
					node n;
					n.type = node::T_SYMBOL;
					n.v_string = tok;
					n.code = to_code(tok);
					ret.push_back(make_snode(n));
				}
			}
			return ret;
		}
	};

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Parses the given s. </summary>
	///
	/// <param name="s">	The std::string to process. </param>
	///
	/// <returns>	A snodelist. </returns>
	snodelist parse(const std::string &s)
	{
		return parser(tokenize(s)).parse();
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Constructor. </summary>
	///
	/// <param name="outer">	The outer. </param>
	environment::environment(senvironment outer) : outer(outer) 
	{
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Gets a snode using the given code. </summary>
	///
	/// <param name="code">	The code to get. </param>
	///
	/// <returns>	A snode. </returns>
	snode environment::get(int code)
	{
		std::map<int, snode>::iterator found = env.find(code);
		if (found != env.end()) {
			return found->second;
		}
		else {
			if (outer != NULL) {
				return outer->get(code);
			}
			else {
				return nil;
			}
		}
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Gets a snode using the given code. </summary>
	///
	/// <param name="k">	[in,out] The k to get. </param>
	///
	/// <returns>	A snode. </returns>
	snode environment::get(snode &k)
	{
		return get(k->code);
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Sets. </summary>
	///
	/// <param name="k">	[in,out] The k to get. </param>
	/// <param name="v">	[in,out] The snode to process. </param>
	///
	/// <returns>	A snode. </returns>
	snode environment::set(snode &k, snode &v)
	{
		return env[k->code] = v;
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Fns. </summary>
	///
	/// <param name="n">			The node to process. </param>
	/// <param name="outer_env">	The outer environment. </param>
	///
	/// <returns>	A snode. </returns>
	snode fn(snode n, std::shared_ptr<environment> outer_env)
	{
		snode n2(n);
		n2->type = node::T_FN;
		n2->outer_env = outer_env;
		return n2;
	}

	// macros
	macromap macros;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Applies the macro. </summary>
	///
	/// <param name="body">	The body. </param>
	/// <param name="vars">	The variables. </param>
	///
	/// <returns>	A snode. </returns>
	snode apply_macro(snode body, std::map<std::string, snode> vars)
	{
		if (body->type == node::T_LIST) {
			snodelist &bvec = body->v_list;
			snodelist ret;
			for (unsigned int i = 0; i < bvec.size(); i++) {
				snode b = bvec.at(i);
				if (b->v_string == "...") {
					snode &vargs = vars[b->v_string];
					ret.insert(ret.end(), vargs->v_list.begin(), vargs->v_list.end());
				}
				else ret.push_back(apply_macro(bvec.at(i), vars));
			}
			return make_snode(ret);
		}
		else {
			std::string &bstr = body->v_string;
			if (vars.find(bstr) != vars.end()) return vars[bstr]; else return body;
		}
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Macroexpands the given n. </summary>
	///
	/// <param name="n">	The node to process. </param>
	///
	/// <returns>	A snode. </returns>
	snode macroexpand(snode n)
	{
		snodelist nList = n->v_list;
		if (macros.find(nList[0]->v_string) != macros.end()) {
			snodelist &macro = macros[nList[0]->v_string];
			std::map<std::string, snode> macrovars;
			snodelist &argsyms = macro[0]->v_list;
			for (unsigned int i = 0; i < argsyms.size(); i++) {
				std::string argsym = argsyms.at(i)->v_string;
				if (argsym == "...") {
					snodelist ellipsis;
					snode n2(make_snode(ellipsis));
					snodelist &ellipsis2 = n2->v_list;
					for (unsigned int i2 = i + 1; i2 < nList.size(); i2++)
						ellipsis2.push_back(nList.at(i2));
					macrovars[argsym] = n2;
					break;
				}
				else {
					macrovars[argsyms.at(i)->v_string] = nList.at(i + 1);
				}
			}
			return apply_macro(macro[1], macrovars);
		}
		else
			return n;
	}

	// ====================================================================
	// C O M P I L E R 
	// ====================================================================

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Compiles the given n. </summary>
	///
	/// <param name="n">	[in,out] The node to process. </param>
	///
	/// <returns>	A snode. </returns>
	snode compile(snode &n)
	{
		switch (n->type) {
		case node::T_LIST: // function (FUNCTION ARGUMENT ..)
		{
			if (n->v_list.size() == 0) return n;
			snode func = compile(n->v_list[0]);
			if (func->type == node::T_SYMBOL && func->v_string == "defmacro") { // (defmacro add (a b) (+ a b)) ; define macro
				snodelist v;
				v.push_back(n->v_list[2]);
				v.push_back(n->v_list[3]);
				macros[n->v_list[1]->v_string] = v;
				return nil;
			}
			else if (func->type == node::T_SYMBOL && func->v_string == "quote") { // ignore macro
				return n;
			}
			else {
				if (macros.find(func->v_string) != macros.end()) {
					snode expanded = macroexpand(n);
					return compile(expanded);
				}
				else {
					snodelist r;
					for (snodelist::iterator i = n->v_list.begin(); i != n->v_list.end(); i++) {
						r.push_back(compile(*i));
					}
					return make_snode(r);
				}
			}
		}
		default: return n;
		}
	}

	typedef std::pair<std::string, std::string> ctxpair;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Getcontexts the given string. </summary>
	///
	/// <param name="str">	The string. </param>
	///
	/// <returns>	A ctxpair. </returns>
	ctxpair getcontext(std::string str)
	{
		std::string ctx = currContext;
		std::string sym = str;
		size_t pos = sym.find(':');
		if (pos != std::string::npos) {
			ctx = sym.substr(0, pos);
			sym = sym.substr(pos + 1);
		}
		return{ ctx, sym };
	}

	// ====================================================================
	// E V A L 
	// ====================================================================


	///-------------------------------------------------------------------------------------------------
	/// <summary>	Evals. </summary>
	///
	/// <exception cref="zi_exception">	Thrown when a zi error condition occurs. </exception>
	///
	/// <param name="n">  	[in,out] The node to process. </param>
	/// <param name="env">	[in,out] The environment. </param>
	///
	/// <returns>	A snode. </returns>
	snode eval(snode &n, senvironment &env)
	{
		switch (n->type) {
		case node::T_SYMBOL:
		{
			ctxpair ctx = getcontext(n->to_string());
			std::string save;
			int isave;
			snode ret = env->get(n);
			
			if (ctx.first != currContext) {
				save = n->v_string;
				isave = n->code;
				n->v_string = ctx.second;
				n->code = to_code(ctx.second);
				if( contexts.find(ctx.first) == contexts.end())
					throw zi_exception("Undefined context (sym): " + ctx.first);

				ret = contexts[ctx.first]->get(n);
				n->v_string = save;
				n->code = isave;
			}
			
			if (ret->type == node::T_NIL) {
				ret = contexts["MAIN"]->get(n);
				if( ret->type == node::T_NIL)	
					throw zi_exception("Symbol not found: " + n->to_string());
			}
			return ret;
		}
		case node::T_LIST: // function (FUNCTION ARGUMENT ..)
		{
			if (n->v_list.size() == 0) return nil;

			snode func = eval(n->v_list[0], env);
			switch (func->type) {
			case node::T_SPECIAL:
			{
				return func->v_builtin(n->v_list, env);
			}
			case node::T_BUILTIN:
			case node::T_FN:
			{
				// evaluate arguments
				snodelist args;
				environment env2(func->outer_env);
				// we'll stick the name in here so we can complain properly
				func->v_string = n->v_list[0]->to_string();
				args.reserve(n->v_list.size() - 1);
				for (auto i = n->v_list.begin() + 1; i != n->v_list.end(); i++) {
					args.push_back(eval(*i, env));
				}
				senvironment senv2(std::make_shared<environment>(env2));
				return apply(func, args, senv2);
			}
			default:
				return n;
			} // end switch (func->type)
		}
		default:
			return n;
		}
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Compile all. </summary>
	///
	/// <param name="lst">	[in,out] The list. </param>
	///
	/// <returns>	A snodelist. </returns>
	snodelist compile_all(snodelist &lst)
	{
		snodelist compiled;
		int last = lst.size() - 1;
		for (int i = 0; i <= last; i++) {
			compiled.push_back(compile(lst[i]));
		}
		return compiled;
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Eval all. </summary>
	///
	/// <param name="lst">	[in,out] The list. </param>
	///
	/// <returns>	A snode. </returns>
	snode eval_all(snodelist &lst)
	{
		int last = lst.size() - 1;
		if (last < 0) return nil;
		for (int i = 0; i < last; i++) {
			eval(lst[i], contexts[currContext]);
		}
		return eval(lst[last], contexts[currContext]);
	}


	///-------------------------------------------------------------------------------------------------
	/// <summary>	Eval string. </summary>
	///
	/// <param name="s">	[in,out] The std::string to process. </param>
	///
	/// <returns>	A snode. </returns>
	snode eval_string(std::string &s)
	{
		snodelist vec = parse(s);
		snodelist compiled = compile_all(vec);
		/*
		for (auto arg : compiled)
			std::cout << "DBG: " << arg->str_with_type() << std::endl;
		*/
		return eval_all(compiled);
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Eval string. </summary>
	///
	/// <param name="s">	The std::string to process. </param>
	///
	/// <returns>	A snode. </returns>
	snode eval_string(const char* s)
	{
		std::string s2(s);
		return eval_string(s2);
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Eval print. </summary>
	///
	/// <param name="s">	[in,out] The std::string to process. </param>
	inline void eval_print(std::string &s)
	{
		std::cout << eval_string(s)->str_with_type() << std::endl;
	}

	// ====================================================================
	// R E P L 
	// ====================================================================

	/// <summary>	Print logo. </summary>
	void print_logo()
	{
		std::cout << "Zi v" << ZI_VERSION << std::endl;
		std::cout << ZI_COPYRIGHT << std::endl;
	}

	/// <summary>	a la python. </summary>
	void prompt()
	{
		std::cout << currContext + ">>> ";
	}

	/// <summary>	Prompt 2. </summary>
	void prompt2()
	{
		std::cout << "... ";
	}

	/// <summary>	read-eval-print loop. </summary>
	int repl()
	{
		std::string code;
		bool done = false;
		std::cout << "'.help' for help" << std::endl;

		while (!done) {
			try {
				if (code.length() == 0) prompt(); else prompt2();
				std::string line;
				if (!std::getline(std::cin, line)) { // EOF
					eval_print(code);
					return 0;
				}

				if (line[0] == '.') {
					if (line == ".exit")
						done = true;
					if (line == ".help" || line == ".?")
						std::cout << ".help .exit" << std::endl;
				}
				else {
					code += '\n' + line;
					tokenizer t(code);
					t.tokenize();
					if (t.unclosed <= 0) { // no unmatched parenthesis nor quotation
						eval_print(code);
						code = "";
					}
				}
			}
			catch (zi::zi_exception& e) {
				std::cout << "Error: " << e.what() << std::endl;
				code = "";
			}
			catch (zi::zi_exit_exception& e) {
				return e.code();
			}
		} // while

		return 0;
	}

	// ====================================================================
	// U T I L I T Y  F U N C S 
	// ====================================================================

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Random double. </summary>
	///
	/// <returns>	A double. </returns>
	inline double rand_double()
	{
		// TODO replace this with the c++ generator
		return (double)rand() / ((double)RAND_MAX + 1.0);
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Gets a snode using the given name. </summary>
	///
	/// <param name="name">	The name. </param>
	///
	/// <returns>	A snode. </returns>
	snode get(const char* name)
	{
		std::string s(name);
		return contexts[currContext]->get(to_code(s));
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Sets a variable </summary>
	///
	/// <param name="name"> 	The name. </param>
	/// <param name="value">	The value. </param>
	void set(const char* name, node value)
	{
		std::string s(name);
		contexts[currContext]->env[to_code(s)] = make_snode(value);
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Sets a variable with user pointer type </summary>
	///
	/// <param name="name">   	The name. </param>
	/// <param name="value">  	The value. </param>
	/// <param name="ptrtype">	The ptrtype. </param>
	void set(const char* name, node value, unsigned int ptrtype)
	{
		std::string s(name);
		auto sn = make_snode(value);
		sn->v_ptrtype = ptrtype;
		contexts[currContext]->env[to_code(s)] = sn;
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	extracts characters from filename and stores them into str. </summary>
	///
	/// <param name="filename">	Filename of the file. </param>
	/// <param name="str">	   	[in,out] The string. </param>
	///
	/// <returns>	true if it succeeds, false if it fails. </returns>
	bool slurp(std::string filename, std::string &str)
	{
		std::ifstream in(filename);
		if (in.is_open()) {
			std::stringstream buffer;
			buffer << in.rdbuf();
			in.close();
			str = buffer.str();
			return true;
		}
		return false;
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Opposite of slurp. Writes str to filename. </summary>
	///
	/// <param name="filename">	Filename of the file. </param>
	/// <param name="str">	   	The string. </param>
	///
	/// <returns>	An int of the number of chars written or -1 on fail </returns>
	int spit(std::string filename, const std::string &str)
	{
		std::ofstream out(filename);
		if (out.is_open()) {
			out << str;
			out.close();
			return str.length();
		}
		return -1;
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	check argument count to builtins. </summary>
	///
	/// <exception cref="zi_exception">	Thrown when a zi error condition occurs. </exception>
	///
	/// <param name="func"> 	The function. </param>
	/// <param name="count">	Number of. </param>
	/// <param name="args"> 	[in,out] The arguments. </param>
	void expect_atleast(std::string func, int count, snodelist &args)
	{
		if ((int)args.size() < count)
			throw zi_exception(func + " expects at least " + std::to_string(count) + (count > 1 ? " args" : " arg"));
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Expect argument. </summary>
	///
	/// <exception cref="zi_exception">	Thrown when a zi error condition occurs. </exception>
	///
	/// <param name="func"> 	The function. </param>
	/// <param name="count">	Number of. </param>
	/// <param name="args"> 	[in,out] The arguments. </param>
	void expect_arg(std::string func, int count, snodelist &args)
	{
		if ((int)args.size() != count)
			throw zi_exception(func + " expects " + std::to_string(count) + (count > 1 ? " args" : " arg"));
	}

	// ====================================================================
	// S P E C I A L S 
	// ====================================================================

	// (def SYMBOL VALUE) ; set in the current environment
	snode special_def(snodelist &raw_args, senvironment &env)
	{
		if (raw_args.size() < 3)
			throw zi_exception("def: expects at least 2 args");

		snode value = make_snode(*eval(raw_args[2], env));
		return env->set(raw_args[1], value);
	}

	// (cond ((IF true) (THEN))... [((else) (THEN))])
	snode special_cond(snodelist &args, senvironment &env)
	{
		for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
			if ((*i)->v_list.size() != 2) {
				throw zi_exception("cond... expected ((test) (result))");
			}
			snode cond = (*i)->v_list[0];
			snode proc = (*i)->v_list[1];
			if (cond->type == node::T_SYMBOL && cond->to_string() == "else") {
				return eval(proc, env);
			}
			else if (eval(cond, env)->v_bool) {
				return eval(proc, env);
			}
		}
		// if none of the conditions are true (no else) it is an error
		throw zi_exception("cond... you have entered the twilight zone");
	}

	// (if CONDITION THEN_EXPR {ELSE_EXPR})
	snode special_if(snodelist &raw_args, senvironment &env)
	{
		snode &cond = raw_args[1];
		snode result = eval(cond, env);
		if (result->v_bool) {
			return eval(raw_args[2], env);
		}
		else {
			if (raw_args.size() < 4) return nil;
			return eval(raw_args[3], env);
		}
	}

	// (set SYMBOL-OR-PLACE VALUE)
	// symbol must already exist??
	snode special_set(snodelist &raw_args, senvironment &env)
	{
		if (raw_args.size() < 3)
			throw zi_exception("set: expects at least 2 args");

		snode value = make_snode(*eval(raw_args[2], env));

		try {
			snode var = eval(raw_args[1], env);
			*var = *value;
			return var;
		}
		catch (zi_exception &) {
			// std::cerr << e.what() << std::endl;
			// TODO prolly should make a specific exception for not found
			// idk whether to catch this or let it slide
			// def and let are basically the same thing but letting
			// this propogate catchs some possible code typos maybe?
			if (raw_args[1]->type == node::T_SYMBOL) { // new variable
				return env->set(raw_args[1], value);
			}
		}
		return nil;
	}

	// (fn (ARGUMENT ..) BODY) => lexical closure
	snode special_fn(snodelist &raw_args, senvironment &env)
	{
		snode n2 = fn(make_snode(raw_args), std::make_shared<environment>(env));
		return n2;
	}

	// (let (VARS...) (BODY...))
	// (let name (VARS...) (BODY...))
	snode special_let(snodelist &raw_args, senvironment &env)
	{
		int len = raw_args.size();
		if (len < 3)
			throw zi_exception("let: expected at least 2 args");

		snode fnname;
		snode inits = raw_args[1];
		snodelist body;
		int body_offs = 2;
		if (raw_args[1]->type == node::T_SYMBOL) {
			fnname = raw_args[1];
			inits = raw_args[2];
			body_offs = 3;
		}
		for (int i = body_offs; i < (int)raw_args.size(); i++) {
			body.push_back(raw_args[i]);
		}

		environment env2(env);
		snodelist params;
		snodelist values;
		for (auto i = inits->v_list.begin(); i != inits->v_list.end(); i++) {
			snode name = (*i)->v_list[0];
			snode code = (*i)->v_list[1];
			snode val = eval(code, env);
			env2.set(name, val);
			params.push_back((*i)->v_list[0]);
			values.push_back((*i)->v_list[1]);
		}

		//
		// named
		// (let loop ((i 0)) (println i) (if (< i 5) (loop (+ i 1))))
		senvironment senv2(std::make_shared<environment>(env2));
		if (body_offs == 3) {
			snodelist func;
			func.push_back(fnname);
			func.push_back(make_snode(node(params)));
			for( auto i : body)
				func.push_back(i);
			snode fnval = fn(make_snode(func), senv2);
			senv2->set(fnname, fnval);
			snodelist call;
			call.push_back(fnname);
			for (auto i : values)
				call.push_back(i);
			snode call_val = make_snode(node(call));
			return eval(call_val, senv2);
		}
		else {
			int last = body.size() - 1;
			if (last < 0) return nil;
			for (int i = 0; i < last; i++)
				eval(body[i], senv2);
			return eval(body[last], senv2);
		}
	}

	// (and X ..) short-circuit
	// the special form 'and' returns a true value if all its subforms are true. The
	// actual value returned is the value of the final subform. If any of the subforms are
	// false, 'and' returns #f		
	snode special_andand(snodelist &raw_args, senvironment &env)
	{
		snode lval = node_true;
		for (snodelist::iterator i = raw_args.begin() + 1; i != raw_args.end(); i++) {
			snode ret = (eval(*i, env));
			if (!ret->v_bool) { return node_false; }
			lval = ret;
		}
		return lval;
	}

	// (or X ..) short-circuit
	// the special form 'or' returns the value of its first true subform. If all the subforms
	// are false, 'or' returns #f
	snode special_oror(snodelist &raw_args, senvironment &env)
	{
		for (snodelist::iterator i = raw_args.begin() + 1; i != raw_args.end(); i++) {
			snode ret = (eval(*i, env));
			if (ret->v_bool) { return (ret); }
		}
		return node_false;
	}

	// (while CONDITION EXPR ..)
	snode special_while(snodelist &raw_args, senvironment &env)
	{
		if (raw_args.size() < 3)
			throw zi_exception("while: expects at least 2 args");

		snode &cond = raw_args[1];
		int len = raw_args.size();
		while (eval(cond, env)->v_bool) {
			for (int i = 2; i < len; i++) {
				eval(raw_args[i], env);
			}
		}
		return nil;
	}

	// (quote X)
	snode special_quote(snodelist &raw_args, senvironment &)
	{
		if (raw_args.size() != 2)
			throw zi_exception("quote: expects 1 arg: got: " + std::to_string(raw_args.size()));

		return raw_args[1];
	}

	// (begin X ..)
	snode special_begin(snodelist &raw_args, senvironment &env)
	{
		int last = raw_args.size() - 1;
		if (last <= 0) return nil;
		for (int i = 1; i < last; i++) {
			eval(raw_args[i], env);
		}
		return eval(raw_args[last], env);
	}

	// ====================================================================
	// B U I L T I N S 
	// ====================================================================

	// (context NAME)
	snode builtin_context(snodelist &args, senvironment &)
	{
		int len = args.size();
		if (len > 0) {
			snode name = args[0];
			if (name->type != node::T_SYMBOL)
				throw zi_exception("context expected a symbol");
			if (contexts.find(name->to_string()) == contexts.end()) {
				contexts[name->to_string()] = std::make_shared<environment>(environment());
			}
			currContext = name->to_string();
		}
		return make_symbol(currContext);
	}

	// exists? SOMETHING
	snode builtin_exists(snodelist &args, senvironment &)
	{
		expect_arg("exists?", 2, args);

		if (args.size() > 1) {
			ctxpair ctx = getcontext(args[1]->to_string());
			snode ret = contexts[ctx.first]->get(args[1]);
			if (ctx.first != currContext) {
				std::string save = args[1]->v_string;
				int isave = args[1]->code;
				args[1]->v_string = ctx.second;
				args[1]->code = to_code(ctx.second);
				if (contexts.find(ctx.first) == contexts.end())
					throw zi_exception("Undefined context (sym): " + ctx.first);

				ret = contexts[ctx.first]->get(args[1]);
				args[1]->v_string = save;
				args[1]->code = isave;
			}

			if (ret->type != node::T_NIL)
				return node_true;
		}
		return node_false;
	}

	// (+ X ..)
	snode builtin_plus(snodelist &args, senvironment &)
	{
		expect_atleast("+", 1, args);

		int len = args.size();
		if (len <= 0) return node_0;
		snode first = args[0];
		if (first->type == node::T_INT || first->type == node::T_CHAR) {
			int sum = first->v_int;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum += (*i)->to_int();
			}
			return make_snode(sum);
		}
		else if (first->type == node::T_STRING) {
			std::string sum = first->v_string;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum += (*i)->to_string();
			}
			return make_snode(sum);
		}
		else {
			double sum = first->v_double;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum += (*i)->to_double();
			}
			return make_snode(sum);
		}
	}

	// (- X ..)
	snode builtin_minus(snodelist &args, senvironment &)
	{
		expect_atleast("-", 1, args);

		int len = args.size();
		if (len <= 0)
			return node_0;
		snode first = args[0];
		if (first->type == node::T_INT || first->type == node::T_CHAR) {
			if (len == 1) return make_snode(first->v_int*-1);
			int sum = first->v_int;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum -= (*i)->to_int();
			}
			return make_snode(sum);
		}
		else {
			if (len == 1) return make_snode(-first->v_double * -1.0);
			double sum = first->v_double;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum -= (*i)->to_double();
			}
			return make_snode(sum);
		}
	}

	// (* X ..)
	snode builtin_mul(snodelist &args, senvironment &)
	{
		expect_atleast("*", 1, args);

		int len = args.size();
		if (len <= 0) return node_1;
		snode first = args[0];
		if (first->type == node::T_INT || first->type == node::T_CHAR) {
			int sum = first->v_int;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum *= (*i)->to_int();
			}
			return make_snode(sum);
		}
		else {
			double sum = first->v_double;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum *= (*i)->to_double();
			}
			return make_snode(sum);
		}
	}

	// (/ X ..)
	snode builtin_div(snodelist &args, senvironment &)
	{
		expect_atleast("/", 1, args);

		int len = args.size();
		if (len <= 0) return node_1;
		snode first = args[0];
		if (first->type == node::T_INT || first->type == node::T_CHAR) {
			int sum = first->v_int;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum /= (*i)->to_int();
			}
			return make_snode(sum);
		}
		else {
			double sum = first->v_double;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				sum /= (*i)->to_double();
			}
			return make_snode(sum);
		}
	}

	// (< X Y)
	snode builtin_lt(snodelist &args, senvironment &)
	{
		expect_arg("<", 2, args);
		if (args[0]->type == node::T_INT || args[0]->type == node::T_CHAR) {
			return make_snode(args[0]->v_int < args[1]->to_int());
		}
		else if (args[0]->type == node::T_STRING) {
			return make_snode(args[0]->to_string() < args[1]->to_string());
		}
		else {
			return make_snode(args[0]->v_double < args[1]->to_double());
		}
	}

	// (max ... | LST )
	snode builtin_max(snodelist &args, senvironment &)
	{
		expect_atleast("max", 1, args);
		if (args[0]->type == node::T_INT || args[0]->type == node::T_CHAR) {
			std::vector<int> v;
			for (snodelist::iterator i = args.begin(); i != args.end(); i++) {
				v.push_back((*i)->to_int());
			}
			return make_snode(*std::max_element(v.begin(), v.end()));
		}
		else if (args[0]->type == node::T_LIST) {
			if (args[0]->v_list[0]->type == node::T_INT) {
				std::vector<int> v;
				for (snodelist::iterator i = args[0]->v_list.begin(); i != args[0]->v_list.end(); i++) {
					v.push_back((*i)->to_int());
				}
				return make_snode(*std::max_element(v.begin(), v.end()));
			}
			else {
				std::vector<double> v;
				for (snodelist::iterator i = args[0]->v_list.begin(); i != args[0]->v_list.end(); i++) {
					v.push_back((*i)->to_double());
				}
				return make_snode(*std::max_element(v.begin(), v.end()));
			}
		}
		else {
			std::vector<double> v;
			for (snodelist::iterator i = args.begin(); i != args.end(); i++) {
				v.push_back((*i)->to_double());
			}
			return make_snode(*std::max_element(v.begin(), v.end()));
		}
	}

	// (min ... | LST)
	snode builtin_min(snodelist &args, senvironment &)
	{
		expect_atleast("min", 1, args);
		if (args[0]->type == node::T_INT || args[0]->type == node::T_CHAR) {
			std::vector<int> v;
			for (snodelist::iterator i = args.begin(); i != args.end(); i++) {
				v.push_back((*i)->to_int());
			}
			return make_snode(*std::min_element(v.begin(), v.end()));
		}
		else if (args[0]->type == node::T_LIST) {
			if (args[0]->v_list[0]->type == node::T_INT) {
				std::vector<int> v;
				for (snodelist::iterator i = args[0]->v_list.begin(); i != args[0]->v_list.end(); i++) {
					v.push_back((*i)->to_int());
				}
				return make_snode(*std::min_element(v.begin(), v.end()));
			}
			else {
				std::vector<double> v;
				for (snodelist::iterator i = args[0]->v_list.begin(); i != args[0]->v_list.end(); i++) {
					v.push_back((*i)->to_double());
				}
				return make_snode(*std::min_element(v.begin(), v.end()));
			}
		}
		else {
			std::vector<double> v;
			for (snodelist::iterator i = args.begin(); i != args.end(); i++) {
				v.push_back((*i)->to_double());
			}
			return make_snode(*std::min_element(v.begin(), v.end()));

		}
	}


	// (expt BASE EXPONENT)
	snode builtin_caret(snodelist &args, senvironment &)
	{
		expect_arg("expt", 2, args);
		return make_snode(pow(args[0]->to_double(), args[1]->to_double()));
	}

	// (modulo DIVIDEND DIVISOR)
	snode builtin_percent(snodelist &args, senvironment &)
	{
		expect_arg("modulo", 2, args);
		return make_snode(args[0]->to_int() % args[1]->to_int());
	}

	// (sqrt X)
	snode builtin_sqrt(snodelist &args, senvironment &)
	{
		expect_arg("sqrt", 1, args);
		return make_snode(sqrt(args[0]->to_double()));
	}

	// (inc X)
	snode builtin_plusplus(snodelist &args, senvironment &)
	{
		expect_arg("inc", 1, args);
		int len = args.size();
		if (len <= 0) return make_snode(0);
		snode first = args[0];
		if (first->type == node::T_INT || args[0]->type == node::T_CHAR) {
			first->v_int++;
		}
		else {
			first->v_double++;
		}
		return first;
	}

	// (dec X)
	snode builtin_minusminus(snodelist &args, senvironment &)
	{
		expect_arg("dec", 1, args);
		int len = args.size();
		if (len <= 0) return make_snode(0);
		snode first = args[0];
		if (first->type == node::T_INT || args[0]->type == node::T_CHAR) {
			first->v_int--;
		}
		else {
			first->v_double--;
		}
		return first;
	}

	// (floor X)
	snode builtin_floor(snodelist &args, senvironment &)
	{
		expect_arg("floor", 1, args);
		return make_snode(floor(args[0]->to_double()));
	}

	// (ceil X)
	snode builtin_ceil(snodelist &args, senvironment &)
	{
		expect_arg("ceil", 1, args);
		return make_snode(ceil(args[0]->to_double()));
	}

	// (ln X)
	snode builtin_ln(snodelist &args, senvironment &)
	{
		expect_arg("ln", 1, args);
		return make_snode(log(args[0]->to_double()));
	}

	// (log10 X)
	snode builtin_log10(snodelist &args, senvironment &)
	{
		expect_arg("log10", 1, args);
		return make_snode(log10(args[0]->to_double()));
	}

	// (sin X)
	snode builtin_sin(snodelist &args, senvironment &)
	{
		expect_arg("sin", 1, args);
		return make_snode(sin(args[0]->to_double()));
	}

	// (cos X)
	snode builtin_cos(snodelist &args, senvironment &)
	{
		expect_arg("cos", 1, args);
		return make_snode(cos(args[0]->to_double()));
	}

	// (tan X)
	snode builtin_tan(snodelist &args, senvironment &)
	{
		expect_arg("tan", 1, args);
		return make_snode(tan(args[0]->to_double()));
	}

	// (asin X)
	snode builtin_asin(snodelist &args, senvironment &)
	{
		expect_arg("asin", 1, args);
		return make_snode(asin(args[0]->to_double()));
	}

	// (acos X)
	snode builtin_acos(snodelist &args, senvironment &)
	{
		expect_arg("acos", 1, args);
		return make_snode(acos(args[0]->to_double()));
	}

	// (atan X)
	snode builtin_atan(snodelist &args, senvironment &)
	{
		expect_arg("atan", 1, args);

		return make_snode(atan(args[0]->to_double()));
	}

	// (rand)
	snode builtin_rand(snodelist &, senvironment &)
	{
		return make_snode(rand_double());
	}

	// (null? LST)
	snode builtin_isnull(snodelist &args, senvironment &env)
	{
		expect_arg("null?", 2, args);

		snode first = eval(args[1], env);
		if (first->type == node::T_LIST) {
			if (first->v_list.size() == 0) return node_true;
		}
		return node_false;
	}

	// (atom? x)
	snode builtin_isatom(snodelist &args, senvironment &env)
	{
		expect_arg("atom?", 2, args);

		snode first = eval(args[1], env);
		if (first->type == node::T_LIST || first->type == node::T_NIL)
			return node_false;
		return node_true;
	}

	// (= X ..) short-circuit
	snode builtin_eqeq(snodelist &args, senvironment &)
	{
		expect_atleast("=", 1, args);

		snode first = args[0];

		if (first->type == node::T_BOOL) {
			bool firstv = first->v_bool;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				if ((*i)->v_bool != firstv) { return node_false; }
			}
		}
		else if (first->type == node::T_INT || first->type == node::T_CHAR) {
			int firstv = first->v_int;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				if ((*i)->to_int() != firstv) { return node_false; }
			}
		}
		else if (first->type == node::T_STRING) {
			std::string firstv = first->to_string();
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				if ((*i)->type != node::T_STRING) { return node_false; }
				else if ((*i)->to_string() != firstv) { return node_false; }
			}
		}
		else if (first->type == node::T_NIL) {
			std::string firstv = first->to_string();
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				if ((*i)->type != node::T_NIL) { return node_false; }
			}
		}
		else if (first->type == node::T_LIST) {
			std::string firstv = first->to_string();
			if (firstv != args[1]->to_string()) { return node_false; }
		}
		else {
			double firstv = first->v_double;
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				if ((*i)->to_double() != firstv) { return node_false; }
			}
		}
		return node_true;
	}

	// (! X)
	snode builtin_not(snodelist &args, senvironment &)
	{
		expect_atleast("not", 1, args);

		return make_snode(!(args[0]->v_bool));
	}

	// (string X ..)
	snode builtin_string(snodelist &args, senvironment &)
	{
		expect_atleast("string", 1, args);

		int len = args.size();
		if (len < 1) return make_snode(std::string());
		std::string acc;
		for (snodelist::iterator i = args.begin(); i != args.end(); i++) {
			acc += (*i)->to_string();
		}
		return make_snode(acc);
	}


	// (chr X)
	snode builtin_chr(snodelist &args, senvironment &)
	{
		expect_arg("chr", 1, args);

		char temp[2] = " ";
		temp[0] = (char)args[0]->to_int();
		return make_snode(std::string(temp));
	}

	// (substring start [len])
	snode builtin_substr(snodelist &args, senvironment &)
	{
		expect_atleast("substring", 1, args);

		std::string first = args[0]->to_string();
		int start = args[1]->to_int();
		int len = first.size();
		if (args.size() > 2) {
			len = args[2]->to_int();
		}
		return make_snode(first.substr(start, len));
	}


	// (double X)
	snode builtin_double(snodelist &args, senvironment &)
	{
		expect_arg("double", 1, args);

		return make_snode(args[0]->to_double());
	}

	// (int X)
	snode builtin_int(snodelist &args, senvironment &)
	{
		expect_arg("int", 1, args);

		return make_snode(args[0]->to_int());
	}

	// (type X)
	snode builtin_type(snodelist &args, senvironment &)
	{
		expect_arg("type", 1, args);

		return make_snode(args[0]->type_str());
	}

	// (eval X)
	snode builtin_eval(snodelist &args, senvironment &env)
	{
		expect_arg("eval", 1, args);

		return eval(args[0], env);
	}

	// (list X ..)
	snode builtin_list(snodelist &args, senvironment &)
	{
		snodelist ret;
		ret.reserve(args.size());
		for (auto &n : args) {
			ret.push_back(n);
		}
		return make_snode(ret);
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Applies this object. </summary>
	///
	/// <exception cref="zi_exception">	Thrown when a zi error condition occurs. </exception>
	///
	/// <param name="func">	[in,out] The function. </param>
	/// <param name="args">	[in,out] The arguments. </param>
	/// <param name="env"> 	[in,out] The environment. </param>
	///
	/// <returns>	A snode. </returns>
	snode apply(snode &func, snodelist &args, senvironment &env)
	{
		if (func->type == node::T_BUILTIN) {
			return func->v_builtin(args, env);
		}
		else if (func->type == node::T_FN) {
			snodelist &f = func->v_list;
			// anonymous function application-> lexical scoping
			// (fn (ARGUMENT ..) BODY ..)
			snodelist &arg_syms = f[1]->v_list;

			std::shared_ptr<environment> local_env(std::make_shared<environment>(environment(func->outer_env)));

			int alen = arg_syms.size();
			if (alen != (int)args.size()) {
				throw zi_exception("'" + func->v_string + "' expected " + std::to_string(alen) + " arguments");
			}
			for (int i = 0; i < alen; i++) { // assign arguments
				snode &k = arg_syms[i];
				local_env->env[k->code] = args[i];
			}

			int last = f.size() - 1;
			if (last < 2) return nil;
			for (int i = 2; i < last; i++) { // body
				eval(f.at(i), local_env);
			}
			return eval(f[last], local_env);
		}
		else {
			return nil;
		}
	}

	// (apply FUNC LIST)
	snode builtin_apply(snodelist &args, senvironment &env)
	{
		expect_arg("apply", 2, args);

		snode func = args[0];
		snodelist lst = args[1]->v_list;
		return apply(func, lst, env);
	}

	// (fold FUNC LIST)
	snode builtin_fold(snodelist &args, senvironment &env)
	{
		expect_arg("fold", 2, args);

		snode f = args[0];
		snode lst = args[1];
		snode acc = lst->v_list[0];
		snodelist args2;
		args2.reserve(2);
		args2.push_back(nil); // first argument
		args2.push_back(nil); // second argument
		for (unsigned int i = 1; i < lst->v_list.size(); i++) {
			args2[0] = acc;
			args2[1] = lst->v_list.at(i);
			acc = apply(f, args2, env);
		}
		return acc;
	}

	// (map FUNC LIST)
	snode builtin_map(snodelist &args, senvironment &env)
	{
		expect_arg("map", 2, args);

		snode f = args[0];
		snode lst = args[1];
		snodelist acc;
		snodelist args2;
		auto len = lst->v_list.size();
		acc.reserve(len);
		args2.push_back(nil);
		for (unsigned int i = 0; i < len; i++) {
			args2[0] = lst->v_list[i];
			acc.push_back(apply(f, args2, env));
		}
		return make_snode(acc);
	}

	// (filter FUNC LIST)
	snode builtin_filter(snodelist &args, senvironment &env)
	{
		expect_arg("filter", 2, args);

		snode f = args[0];
		snode lst = args[1];
		snodelist acc;
		snodelist args2;
		args2.push_back(nil);
		for (unsigned int i = 0; i < lst->v_list.size(); i++) {
			snode item = lst->v_list.at(i);
			args2[0] = item;
			snode ret = apply(f, args2, env);
			if (ret->v_bool) acc.push_back(item);
		}
		return make_snode(acc);
	}

	// (push-back! LIST ITEM) ; destructive
	snode builtin_push_backd(snodelist &args, senvironment &)
	{
		expect_arg("push-back!", 2, args);

		args[0]->v_list.push_back(make_snode(*args[1]));
		return args[0];
	}

	// (pop-back! LIST) ; destructive
	snode builtin_pop_backd(snodelist &args, senvironment &)
	{
		expect_arg("pop-back!", 1, args);

		auto &v = args[0]->v_list;
		snode n = v.back();
		v.pop_back();
		return n;
	}

	// (nth INDEX LIST)
	snode builtin_nth(snodelist &args, senvironment &)
	{
		expect_arg("nth", 2, args);

		if (args[1]->type == node::T_STRING) {
			return make_snode(args[1]->v_string[args[0]->to_int()]);
		}
		else {
			return args[1]->v_list[args[0]->to_int()];
		}
	}

	// (length LIST)
	snode builtin_length(snodelist &args, senvironment &)
	{
		expect_arg("length", 1, args);

		if (args[0]->type == node::T_STRING) {
			return make_snode((int)args[0]->v_string.size());
		}
		else if (args[0]->type == node::T_LIST) {
			return make_snode((int)args[0]->v_list.size());
		}
		return nil;
	}

	// (car LST): return first item from list
	snode builtin_car(snodelist &args, senvironment &)
	{
		expect_arg("car", 1, args);

		if (args[0]->type == node::T_LIST) {
			return make_snode(*args[0]->v_list[0]);
		}
		return nil;
	}

	// (cdr LST): returns list save the first item
	snode builtin_cdr(snodelist &args, senvironment &)
	{
		expect_arg("cdr", 1, args);

		snodelist r;
		if (args[0]->type == node::T_LIST) {
			for (snodelist::iterator i = args[0]->v_list.begin() + 1; i != args[0]->v_list.end(); i++) {
				r.push_back(*i);
			}
			return make_snode(r);
		}
		return nil;
	}

	// (append LST ...): returns list save the first item
	snode builtin_append(snodelist &args, senvironment &)
	{
		expect_atleast("append", 2, args);

		snodelist r;
		if (args[0]->type == node::T_LIST) {
			for (snodelist::iterator i = args.begin(); i != args.end(); i++) {
				r.insert(r.end(), (*i)->v_list.begin(), (*i)->v_list.end());
			}
			return make_snode(r);
		}
		return nil;
	}



	// (cons X LST): Returns a new list where x is the first element 
	// and lst is the rest.
	snode builtin_cons(snodelist &args, senvironment &)
	{
		expect_arg("cons", 2, args);

		snode x = args[0];
		snode lst = args[1];
		snodelist r;
		r.push_back(x);
		if (lst->type == node::T_LIST) {
			for (snodelist::iterator i = lst->v_list.begin(); i != lst->v_list.end(); i++) {
				r.push_back(*i);
			}
		}
		else {
			for (snodelist::iterator i = args.begin() + 1; i != args.end(); i++) {
				r.push_back(*i);
			}
		}
		return make_snode(r);
	}

	// (current-time)
	snode builtin_time(snodelist &, senvironment &)
	{
		std::time_t result = std::time(nullptr);
		return make_snode((int)result);
	}

	// (load FILENAME)
	// load and eval
	snode builtin_load(snodelist &args, senvironment &)
	{
		expect_arg("load", 1, args);

		std::string code;
		std::string fname = args[0]->to_string();
		if (zi::slurp(fname, code)) {
			try {
				std::string saveCtx = currContext;
				zi::eval_string(code);
				currContext = saveCtx;
				return node_true;
			}
			catch (zi_exception &) {
				//std::cout << "Exception parsing file: " << fname << ": " << e.what() << std::endl;
				return node_false;
			}
			catch (std::exception &) {
				//std::cout << "std::exception loading file: " << fname << ": " << e.what() << std::endl;
				return node_false;
			}
		}
		else {
			return node_false;
		}
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	try and load rc from a list of paths. </summary>
	///
	/// <param name="paths">	list of paths to try. </param>
	///
	/// <returns>	true if it succeeds, false if it fails. </returns>
	bool loadrc(strlist paths)
	{
		for (auto path : paths) {
			//std::cout << path << std::endl;
			// try and file the .zirc file
			std::string ini;
			if (slurp(path, ini)) {
				eval_string(ini);
				return true;
			}
		}
		return false;
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	get user home path. </summary>
	///
	/// <returns>	A std::string. containing the user home path </returns>
	std::string homeLocation()
	{
		std::string path;
#ifdef _WIN32
#pragma warning(disable: 4996)
		path = getenv("HOMEDRIVE");
		path += getenv("HOMEPATH");
		std::replace(path.begin(), path.end(), '\\', '/');
#else
		path = getenv("HOME");
#endif
		return path;
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	try release all our memory. </summary>
	void cleanup()
	{
		for (auto c : contexts)
			c.second->env.clear();
		contexts.clear();
		macros.clear();
		symname.clear();
	}

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Initialize the global environment. </summary>
	///
	void init()
	{
		srand((unsigned int)time(0));

		senvironment global_env = std::make_shared<environment>(environment());

		global_env->env[to_code("zi.build")] = make_snode(std::string(ZI_BUILD));
		global_env->env[to_code("zi.version")] = make_snode(std::string(ZI_VERSION));
		global_env->env[to_code("zi.library")] = make_snode(homeLocation() + "/.zi/lib/");

		global_env->env[to_code("true")] = make_snode(true);
		global_env->env[to_code("false")] = make_snode(false);

		global_env->env[to_code("def")] = make_special(special_def);
		global_env->env[to_code("set")] = make_special(special_set);
		global_env->env[to_code("if")] = make_special(special_if);
		global_env->env[to_code("fn")] = make_special(special_fn);
		global_env->env[to_code("begin")] = make_special(special_begin);
		global_env->env[to_code("while")] = make_special(special_while);
		global_env->env[to_code("cond")] = make_special(special_cond);
		global_env->env[to_code("quote")] = make_special(special_quote);
		global_env->env[to_code("let")] = make_special(special_let);

		global_env->env[to_code("context")] = make_snode(builtin_context);

		// ops
		global_env->env[to_code("eval")] = make_snode(builtin_eval);
		global_env->env[to_code("+")] = make_snode(builtin_plus);
		global_env->env[to_code("-")] = make_snode(builtin_minus);
		global_env->env[to_code("*")] = make_snode(builtin_mul);
		global_env->env[to_code("/")] = make_snode(builtin_div);
		global_env->env[to_code("<")] = make_snode(builtin_lt);

		// maths
		global_env->env[to_code("E")] = make_snode(2.71828182845904523536);
		global_env->env[to_code("PI")] = make_snode(3.14159265358979323846);

		global_env->env[to_code("expt")] = make_snode(builtin_caret);
		global_env->env[to_code("modulo")] = make_snode(builtin_percent);
		global_env->env[to_code("sqrt")] = make_snode(builtin_sqrt);
		global_env->env[to_code("inc")] = make_snode(builtin_plusplus);
		global_env->env[to_code("dec")] = make_snode(builtin_minusminus);
		global_env->env[to_code("floor")] = make_snode(builtin_floor);
		global_env->env[to_code("ceil")] = make_snode(builtin_ceil);
		global_env->env[to_code("ln")] = make_snode(builtin_ln);
		global_env->env[to_code("log10")] = make_snode(builtin_log10);
		global_env->env[to_code("rand")] = make_snode(builtin_rand);
		global_env->env[to_code("sin")] = make_snode(builtin_sin);
		global_env->env[to_code("cos")] = make_snode(builtin_cos);
		global_env->env[to_code("tan")] = make_snode(builtin_tan);
		global_env->env[to_code("asin")] = make_snode(builtin_asin);
		global_env->env[to_code("acos")] = make_snode(builtin_acos);
		global_env->env[to_code("atan")] = make_snode(builtin_atan);
		global_env->env[to_code("max")] = make_snode(builtin_max);
		global_env->env[to_code("min")] = make_snode(builtin_min);

		// tests
		global_env->env[to_code("=")] = make_snode(builtin_eqeq);
		global_env->env[to_code("not")] = make_snode(builtin_not);
		global_env->env[to_code("and")] = make_special(special_andand);
		global_env->env[to_code("or")] = make_special(special_oror);
		global_env->env[to_code("null?")] = make_special(builtin_isnull);
		global_env->env[to_code("atom?")] = make_special(builtin_isatom);
		global_env->env[to_code("exists?")] = make_special(builtin_exists);

		// string stuff 
		// needs: 
		// (string-split str [sep]) -> list
		// (string-join list [sep])
		// (string-replace str from to)
		global_env->env[to_code("chr")] = make_snode(builtin_chr);
		global_env->env[to_code("string")] = make_snode(builtin_string);
		global_env->env[to_code("substring")] = make_snode(builtin_substr);
		//global_env->env[to_code("read-string")] = make_snode(builtin_read_string);

		global_env->env[to_code("int")] = make_snode(builtin_int);
		global_env->env[to_code("double")] = make_snode(builtin_double);
		global_env->env[to_code("list")] = make_snode(builtin_list);
		global_env->env[to_code("type")] = make_snode(builtin_type);

		global_env->env[to_code("append")] = make_snode(builtin_append);
		global_env->env[to_code("apply")] = make_snode(builtin_apply);
		global_env->env[to_code("fold")] = make_snode(builtin_fold);
		global_env->env[to_code("map")] = make_snode(builtin_map);
		global_env->env[to_code("filter")] = make_snode(builtin_filter);
		global_env->env[to_code("push-back!")] = make_snode(builtin_push_backd);
		global_env->env[to_code("pop-back!")] = make_snode(builtin_pop_backd);
		global_env->env[to_code("nth")] = make_snode(builtin_nth);
		global_env->env[to_code("length")] = make_snode(builtin_length);

		global_env->env[to_code("cons")] = make_snode(builtin_cons);
		global_env->env[to_code("car")] = make_snode(builtin_car);
		global_env->env[to_code("cdr")] = make_snode(builtin_cdr);

		global_env->env[to_code("current-time")] = make_snode(builtin_time);

		global_env->env[to_code("load")] = make_snode(builtin_load);

		contexts["MAIN"] = global_env;
		currContext = "MAIN";

		// other modules that that are not useful for embedded
#ifndef ZI_EMBEDDED
		init_os();
#endif
	}
} // namespace zi

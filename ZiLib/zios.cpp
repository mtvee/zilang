// zi os type stuff
// these are functions that are not needed in the scripting language
// but are handy for other things
// -----------------------------------------------------------------

#include <algorithm>
#include "zi.h"

#ifdef _MSC_VER
// stop VC++ from whining about secure CRT stuff
#pragma warning(disable : 4996)
#endif

namespace zi
{
	// print out the current environment
	snode builtin_dump(snodelist &, senvironment &)
	{
		strlist v;

		for (std::map<int, snode>::iterator iter = contexts[currContext]->env.begin(); iter != contexts[currContext]->env.end(); iter++) {
			v.push_back(symname[iter->first]);
		}
		std::sort(v.begin(), v.end());

		std::cout << "Symbols:" << std::endl;
		for (std::vector<std::string>::iterator iter = v.begin(); iter != v.end(); iter++) {
			std::cout << " " << (*iter);
		}
		std::cout << std::endl;

		std::cout << "Macros:" << std::endl;
		print_map_keys(macros);

		if (currContext == "MAIN") {
			std::cout << "Contexts:" << std::endl;
			for (auto ctx : contexts) {
				std::cout << " " << ctx.first;
			}
		}
		std::cout << std::endl;
		return nil;
	}

	// (read-line) : read line from stdin or an istream
	snode builtin_read_line(snodelist &args, senvironment &)
	{
		std::istream *in = &std::cin;

		if (args.size() > 0) {
			if (args[0]->type == node::T_ISTREAM) {
				if (args[0]->v_rawptr == nullptr)
					throw zi_exception("read-line: file is closed");
				in = static_cast<std::ifstream *>(args[0]->v_rawptr);
			}
		}

		std::string line;
		if (!std::getline(*in, line)) { // EOF								
			return nil;
		}
		else {
			return make_snode(line);
		}
	}

	// (slurp FILENAME)
	snode builtin_slurp(snodelist &args, senvironment &)
	{
		expect_arg("slurp", 1, args);

		std::string filename = args[0]->to_string();
		std::string str;
		if (slurp(filename, str))
			return make_snode(str);
		else
			return node_false;
	}

	// (spit FILENAME STRING)
	snode builtin_spit(snodelist &args, senvironment &)
	{
		expect_arg("spit", 2, args);

		std::string filename = args[0]->to_string();
		std::string str = args[1]->to_string();
		return make_snode(spit(filename, str));
	}

	// (open "fname" ["mode"])
	snode builtin_open(snodelist &args, senvironment &)
	{
		expect_atleast("open", 1, args);

		std::string fname = args[0]->to_string();
		std::string mode = "input";
		if (args.size() > 1)
			mode = args[1]->to_string();

		if (mode[0] == 'o') {
			std::ofstream *strm = new std::ofstream(fname);
			if (!strm->is_open()) {
				delete strm;
				return node_false;
			}
			return make_snode(strm);
		}
		if (mode[0] == 'a') {
			std::ofstream *strm = new std::ofstream(fname, std::ios::app);
			if (!strm->is_open()) {
				delete strm;
				return node_false;
			}
			return make_snode(strm);
		}
		else {
			std::ifstream *strm = new std::ifstream(fname);
			if (!strm->is_open()) {
				delete strm;
				return node_false;
			}
			return make_snode(strm);
		}

		//return nil;
	}

	// (close stream)
	snode builtin_close(snodelist &args, senvironment &)
	{
		expect_arg("close", 1, args);

		if (args[0]->type == node::T_OSTREAM) {
			if (args[0]->v_rawptr != nullptr) {
				std::ofstream *ostrm = static_cast<std::ofstream *>(args[0]->v_rawptr);
				ostrm->close();
				delete ostrm;
				args[0]->v_rawptr = nullptr;
			}
			return node_true;
		}
		else if (args[0]->type == node::T_ISTREAM) {
			if (args[0]->v_rawptr != nullptr) {
				std::ifstream *istrm = static_cast<std::ifstream *>(args[0]->v_rawptr);
				istrm->close();
				delete istrm;
				args[0]->v_rawptr = nullptr;
			}
			return node_true;
		}
		else {
			return node_false;
		}
	}

	// (print X ..)
	snode builtin_pr(snodelist &args, senvironment &)
	{
		std::ostream *out = &std::cout;
		bool offs = 0;

		if (args.size() > 0) {
			if (args[0]->type == node::T_OSTREAM) {
				if (args[0]->v_rawptr == nullptr)
					throw zi_exception("print: file is closed");
				out = static_cast<std::ofstream *>(args[0]->v_rawptr);
				offs = 1;
			}
		}

		snodelist::iterator first = args.begin() + offs;
		for (snodelist::iterator i = first; i != args.end(); i++) {
			if (i != first)
				*out << " ";
			*out << (*i)->to_string();
		}
		return nil;
	}

	// (println X ..)
	snode builtin_prn(snodelist &args, senvironment &env)
	{
		std::ostream *out = &std::cout;
		if (args.size() > 0) {
			if (args[0]->type == node::T_OSTREAM) {
				if (args[0]->v_rawptr == nullptr)
					throw zi_exception("print: file is closed");
				out = static_cast<std::ofstream *>(args[0]->v_rawptr);
			}
		}

		builtin_pr(args, env);
		*out << std::endl;
		return nil;
	}

	// (exit {X})
	snode builtin_exit(snodelist &args, senvironment &)
	{
		//std::cout << "bye" << std::endl;

		cleanup();

		// calling, of course, does not free memory properly
		if (args.size() == 0)
			throw zi_exit_exception(0);
		
		throw zi_exit_exception(args[0]->to_int());
		//return nil;
	}


	// (system X ..) ; Invokes the command processor to execute a command.
	snode builtin_system(snodelist &args, senvironment &)
	{
		expect_arg("system", 1, args);

		std::string cmd;
		for (snode &n : args) {
			cmd += n->to_string();
		}
		return make_snode(system(cmd.c_str()));
	}


	/// <summary>	Init operating system. </summary>
	void init_os()
	{
		senvironment global_env = contexts["MAIN"];

		global_env->env[to_code("dump")] = make_snode(builtin_dump);
		global_env->env[to_code("read-line")] = make_snode(builtin_read_line);
		global_env->env[to_code("read-file")] = make_snode(builtin_slurp);
		global_env->env[to_code("write-file")] = make_snode(builtin_spit);
		global_env->env[to_code("open")] = make_snode(builtin_open);
		global_env->env[to_code("close")] = make_snode(builtin_close);
		global_env->env[to_code("print")] = make_snode(builtin_pr);
		global_env->env[to_code("println")] = make_snode(builtin_prn);
		global_env->env[to_code("exit")] = make_snode(builtin_exit);
		global_env->env[to_code("system")] = make_snode(builtin_system);
		global_env->env[to_code("getenv")] = make_snode(
			static_cast<builtin>([](snodelist &args, senvironment &) {
			return make_snode(std::string(getenv(args[0]->to_string().c_str())));
		}));

	}

} // namespace zi

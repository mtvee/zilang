;; some unit testing
;; run in notepad++
;;  cmd /K ""C:\PATH_TO_EXE\nLisp.exe" -i "$(FULL_CURRENT_PATH)""
(println "zI unit tests")
(println "-------------")

(if (not (load "unit.zil")) (println "can't load unit.zil"))

;; TESTS
;; from Peter Norvig's python lisp http://norvig.com/lispy.html
(unit:assert "add"
    (+ 2 2) 4)
(unit:assert "mult"
    (+ (* 2 100) (* 1 10)) 210)
(unit:assert "if>"
    (if (> 6 5) (+ 1 1) (+ 2 2)) 2)
(unit:assert "if<"
    (if (< 6 5) (+ 1 1) (+ 2 2)) 4)
(unit:assert "or"
    (or 1 2 3) 1)
(unit:assert "or"
    (or (= 1 2) (+ 2 2)) 4)
(unit:assert "and"
    (and 1 2 3) 3)
(unit:assert "and"
    (and 1 2 (+ 3 1)) 4)
(unit:assert "define"
    (define x 3) 3)
(unit:assert "definex"
    x 3)
(unit:assert "definex"
    (+ x x) 6)
(unit:assert "begin"
    (begin (define x 1) (set! x (+ x 1)) (+ x 1)) 3)
(unit:assert "lambda"
    ((lambda (x) (+ x x)) 5) 10)
(unit:assert "twice"
    (type (define twice (lambda (x) (* 2 x)))) "fn")
(unit:assert "twicex"
    (twice 5) 10)
(unit:assert "compose"
    (type (define compose (lambda (f g) (lambda (x) (f (g x)))))) "fn")
(unit:assert "composex"
    (nth 0 ((compose list twice) 5)) 10)
(unit:assert "twicex"
    (type (define repeat (lambda (f) (compose f f)))) "fn")
(unit:assert "twicex"
    ((repeat twice) 5) 20)
(unit:assert "twicex"
    ((repeat (repeat twice)) 5) 80)
(unit:assert "twicex"
    (type (define fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1))))))) "fn")
(unit:assert "twicex"
    (fact 3) 6)
(unit:assert "twicex"
    (fact 12) 479001600)
(unit:assert "abs"
    (type (define abs (lambda (n) ((if (> n 0)+ -) 0 n)))) "fn")
(unit:assert "absx"
    (list (abs -3) (abs 0) (abs 3)) (3 0 3))
(unit:assert "combine"
    (type (define combine (lambda (f)
                (lambda (x y)
                  (if (null? x) (quote ())
                      (f (list (car x) (car y))
                     ((combine f) (cdr x) (cdr y)))))))) "fn")
(unit:assert "zip"
    (type (define zip (combine cons))) "fn")
(unit:assert "zipx"
    (string (zip (list 1 2 3 4) (list 5 6 7 8))) (string ((1 5) (2 6) (3 7) (4 8))))
(unit:assert "shuffle"
    (type (define riff-shuffle (lambda (deck) 
                     (begin
                       (define take (lambda (n seq) 
                              (if (<= n 0) (quote ()) 
                              (cons (car seq) (take (- n 1) (cdr seq))))))
                       (define drop (lambda (n seq) 
                              (if (<= n 0) seq (drop (- n 1) (cdr seq)))))
                       (define mid (lambda (seq) 
                             (/ (length seq) 2)))
                       ((combine append) 
                    (take (mid deck) deck) 
                    (drop (mid deck) deck)))))) "fn")

(unit:assert "riff"
    (riff-shuffle (list 1 2 3 4 5 6 7 8)) (1 5 2 6 3 7 4 8))
(unit:assert "riff2"
    (riff-shuffle (riff-shuffle (riff-shuffle (list 1 2 3 4 5 6 7 8)))) (1 2 3 4 5 6 7 8)) 

;; let
(unit:assert "let"
	(let ((x 2) (y 3)) (let ((x 7) (z (+ x y))) (* z x))) 35)

;; little schemer stuff
(defn lat? (lst)
    (cond
        ((null? lst) true)
        ((atom? (car lst))(lat? (cdr lst)))
        (else false)))

(unit:assert "lat?"
          (lat? ( 1 2 3 )) true)

(unit:assert "lat?"
          (lat? ( 1 ( 2 3 ) )) false)


(defn member? (a lst)
    (cond
        ((null? lst) false)
        (else (or (= (car lst) a)
            (member? a (cdr lst))))))

(unit:assert "member?"
    (member? 2 (1 2 3 4)) true)
(unit:assert "member?"
    (member? 6 (1 2 3 4)) false)


(defn rember (a lst)
    (cond
        ((null? lst) (quote ()))
        (else (cond
            ((= (car lst) a) (cdr lst))
            (else (cons (car lst)
                (rember a (cdr lst))))))))

(unit:assert "rember"
    (rember 2 (1 2 3)) (1 3))

;; TESTS DONE
(println "all done! " unit:error-count " failed of " (+ unit:error-count unit:pass-count) " total tests")

;; vim: set ft=zil:


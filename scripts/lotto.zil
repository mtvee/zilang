;;
;; random lotto number generator

(defn sort-list (numList)
    (cond
      ((null? numList) (quote ()))
      ((= (car numList) (apply min numList))
          (cons (car numList) (sort-list (cdr numList))))
          (else (sort-list (append (cdr numList) (list (car numList))))
      )))


(defn shuffle (lst)
  (set len (length lst))
  (for i 0 (- len 2) 1
    (set ci (+ 1 i (floor (* (rand) (- len i 1)))))
    (set c (nth i lst))
    (set (nth i lst) (nth ci lst))
    (set (nth ci lst) c))
  lst)

(defn take (n lst)
  (set r (list))
  (for i 0 (dec n) 1
    (set r (cons (nth i lst) r)))
  r)

(defn lotto ()
  (set r (range 1 49 1))
  (shuffle r)
  (take 6 r))

(println "Here are your lucky numbers: " (sort-list (lotto)))



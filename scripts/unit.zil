;; unit testing
(context 'unit)

(def error-count 0)
(def pass-count 0)

;; spit out an error
(defn report-error (msg a b)
  (inc error-count)
  (println "ERROR: " msg  " expected: " b "(" (type b) ") got: " a  "(" (type a) ")"))

;; test something
(defn assert (msg a b) 
  (if (not (= a b)) 
      (report-error msg a b) 
      (inc pass-count)))


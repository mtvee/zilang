#! this is a special comment for bash and what not, must be first
;; Comments
;; Single line comments start with a semicolon or a pound sign
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 1. Primitive Datatypes and Operators
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* numbers")
;;; Numbers
123				; int
3.14			; double
6.02e+23		; double
(int 3.14)		; => 3 : int
(double 123)	; => 123 : double

;; Function application is written (f x y z ...)
;; where f is a function and x, y, z, ... are operands
;; If you want to create a literal list of data, use (quote) to stop it from
;; being evaluated
(quote (+ 1 2)) ; => (+ 1 2)
; you can also use ' for a symbol but it doesn't work for lists and strings
'a ; => a
;; Now, some arithmetic operations
(+ 1 1)			; => 2
(- 8 1)			; => 7
(* 10 2)		; => 20
(expt 2 3)		; => 8
(/ 5 2)			; => 2
(modulo 5 2)	; => 1
(/ 5.0 2)		; => 2.5

;;; Booleans
true										; for true
false										; for false
(not true)									; => false
(and true false (print "doesn't get here")) ; => false
(or false true (print "doesn't get here"))	; => true

;;; Characters are ints.
(nth 0 "A") ; => 65
(chr 65)	; => "A"

;;; Strings are fixed-length array of characters.
"Hello, world!"
"Benjamin \"Bugsy\" Siegel"		; backslash is an escaping character
"Foo\tbar\r\n"					; includes C escapes: \t \r \n

;; Strings can be added too!
(+ "Hello " "world!")			; => "Hello world!"

;; A string can be treated like a list of characters
(nth 0 "Apple")					; => 65

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 2. Variables
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; You can create or set a variable using (set) or (def)
;; a variable name can use any character except: ();#"
(println "* variables")
(define some-var 5) ; => 5
some-var			; => 5

;; Accessing a previously unassigned variable is an exception
; x					; => Unknown variable: x : nil

;; Local binding: Use lambda calculus! `a` and `b` are bound 
;; to `1` and `2` only within the (fn ...)
;; (there is also `let` for scoping)
((fn (a b) (+ a b)) 1 2) ; => 3

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 3. Collections
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* lists")

;;; Lists
;; Lists are vector-like data structures. (Random access is O(1).)
(cons 1 (cons 2 (cons 3 (list)))) ; => (1 2 3)
;; `list' is a convenience variadic constructor for lists
(list 1 2 3) ; => (1 2 3)
;; and a quote can also be used for a literal list value
(quote (+ 1 2)) ; => (+ 1 2)
;; short form
(println '(+ 1 2)) ; => (+ 1 2)

;; Can still use `cons' to add an item to the beginning of a list
(cons 0 (list 1 2 3)) ; => (0 1 2 3)

;; Lists are a very basic type, so there is a *lot* of functionality for
;; them, a few examples:
(map inc (list 1 2 3))          ; => (2 3 4)
(filter (fn (x) (= 0 (modulo x 2))) (list 1 2 3 4))    ; => (2 4)
(length (list 1 2 3 4))     ; => 4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 3. Functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* functions")

;; Use `fn' to create functions.
;; A function always returns the value of its last expression
(fn () "Hello World") ; => (fn () Hello World) : fn

;; Use parentheses to call all functions, including a lambda expression
((fn () "Hello World")) ; => "Hello World"

;; Assign a function to a var
(def hello-world (fn () "Hello World"))
(hello-world) ; => "Hello World"

;; You can shorten this using the function definition syntactic sugar:
(defn hello-world2 () "Hello World")

;; The () in the above is the list of arguments for the function
(def hello
  (fn (name)
    (+ "Hello " name)))
(hello "there") ; => "Hello there"

;; ... or equivalently, using a sugared definition:
;; note: the last statement in an fn is what get returned
;; so there is not explicit 'return' keyword
(defn hello2 (name)
  (+ "Hello " name))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 4. Equality
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* equality")

;; use `=' for nearly anything
(= 3 3.0) ; => true
(= 2 1) ; => false

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 5. Control Flow
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* control")
;;; Conditionals

(if true				; test expression
    "this is true"		; then expression
    "this is false")	; else expression
; => "this is true"

;;; Loops

;; for loop is for numbers
;; (for SYMBOL START END STEP EXPR ..)
(for i 0 10 2 (print i "")) ; => prints 0 2 4 6 8 10
(println)
(for i 0.0 10 2.5 (print i "")) ; => prints 0 2.5 5 7.5 10
(println)

;; while loop
((fn (i)
  (while (< i 10)
    (print i)
    (inc i))) 0) ; => prints 0123456789
(println)

;; `named let` allows a nice clean recursion
(println 
    (let loop ((numbers '(3 -2 1 6 -5)) (nonneg '()) (neg '()))
    (cond
        ((null? numbers) (list nonneg neg))
        ((>= (car numbers) 0)
            (loop (cdr numbers)
                (cons (car numbers) nonneg) neg))
        ((< (car numbers) 0)
            (loop (cdr numbers) nonneg (cons (car numbers) neg))))))


;; cond
;; (cond ((test)(stmt)) ((test)(stmt))... [(else (stmt))])
;; cond executes each test in each block, if a test returns 'true', the stmt
;; is run. If all statements return false and there is an 'else' clause, it
;; runs that. If none are true and there is no else, an exception is raised
(cond 
	((= 1 2) (println "1 == 2! ugh"))
	((= 1 3) (println "1 == 3! UGH"))
	(else (println "cond is working just fine")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 6. Mutation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* mutation")

;; Use `set' or 'def' to assign a new value to a variable 
;; or a place
(def n 5) ; => 5
(set n (inc n)) ; => 6
n ; => 6
(def a (list 1 2)) ; => (1 2)
(def (nth 0 a) 3) ; => 3
a ; => (3 2)

;; or let to keep variables out of the global space
;; notice there are two x's here, one in the outer
;; let and one in the inner let
(let ((x 2) (y 3)) 
    (let ((x 7) (z (+ x y))) 
        (* z x)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 7. Math
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* math")

; a- versions are available too
(sin 0.34)		; => 0.3334870921408144
(cos 0.34)		; => 0.9427546655283462
(tan 0.34)      ; => 0.3537368780391226

(max 1 2 3 4 5) ; => 5
(min 1 2 3 4 5) ; => 1
; operate on lists too
(max (list 1 2 3 4 5)) ; => 5
(min (list 1 2 3 4 5)) ; => 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 8. Macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* macros")

;; Macros let you extend the syntax of the language.
;; In fact, (defn) is a macro.
(defmacro setfn (name ...) (set name (fn ...)))
(defmacro defn (name ...) (def name (fn ...)))

;; Let's add an infix notation
(defmacro infix (a op ...) (op a ...))
(infix 1 + 2 (infix 3 * 4)) ; => 15

;; Macros are not hygienic, you can clobber existing variables!
;; They are simply code transformations.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 9. Contexts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* contexts")
;; contexts are like modules, self-contained environments 
;; MAIN context is where everything starts and always exists
; switch to a `foo` context, if it doesn't exist it is created
(context 'foo)
; define bar in foo
(def bar 10)
; switch back to MAIN
(context 'MAIN)
; nothing here in MAIN
(println (exists? bar))
; there it is
(println (exists? foo:bar))
(println foo:bar)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 10. Other stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(println "* other stuff")

; 5 ways to calc fibonacci
; ---------------------------
(defn fib (n)
  (if (<= n 2)
      1
      (+ (fib (- n 1)) (fib (- n 2)))))

(println (fib 10))

; another way
; ---------------------------
(defn fib (n)
    (cond
      ((= n 0) 0)
      ((= n 1) 1)
      (else
        (+ (fib (- n 1))
           (fib (- n 2))))))

(println (fib 10))

; funky
; ---------------------------
(defn fib-iter (a b count)
  (if (= count 0)
      b
      (fib-iter (+ a b) a (- count 1))))

(defn fib (n)
  (fib-iter 1 0 n))
 
(println (fib 10))

; no cond
; ---------------------------
(defn fib (n)
  (if (< n 2)
      n
      (+ (fib (- n 1))
         (fib (- n 2)))))

(println (fib 10))

; tail recurse
; ---------------------------
(defn fib (n)
  (let loop ((a 0) (b 1) (n n))
    (if (= n 0) a
        (loop b (+ a b) (- n 1)))))

(println (fib 10))

; vim: set filetype=zil:

;; tower of hanoi
;; https://en.wikipedia.org/wiki/Tower_of_Hanoi
(defn hanoi (n a b c)
  (if (> n 0)
    (begin
      (hanoi (- n 1) a c b)
      (println "Move disk from pole " a " to pole " b)
      (hanoi (- n 1) c b a))))
 
(hanoi 4 1 2 3)

// This tells Catch to provide a main() - only do this in one cpp file

#include "catch.h"
#include "zi.h"

TEST_CASE("General Zi startup", "[zi::init]") 
{
	zi::init();
	zi::snode ret = zi::eval_string("(+ 1 1)");
	REQUIRE(ret->to_int() == 2);
}

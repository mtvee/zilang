#include "catch.h"
#include "cli.h"

TEST_CASE("Test cli parsing", "[cli::]") 
{
	cli::Options opt;
	opt.addOpt( "mcl", cli::OptType::Flag );
	opt.addOpt( "bar", cli::OptType::String );
	opt.addOpt( "fud", cli::OptType::String );
	int argc = 7;
// FIXME
#pragma GCC diagnostic ignored "-Wwrite-strings"
    char *argv[] = { 
		"$progname", 
		"-mcl", 
		"----bar", 
		"foo", 
		"--fud=bleh", 
		"fizz", 
		"buzz" 
	};
	opt.parse(argc, argv);
#pragma GCC diagnostic pop	
	REQUIRE(opt.hasFlag("mcl"));// true 
	std::string val = opt.getValue("bar");// "foo"
	REQUIRE(val == "foo");
	cli::strlist vals = opt.getValues("bar");// {"foo"}
	REQUIRE(vals.size() == 1);
	REQUIRE(vals[0] == "foo");
	std::string val2 = opt.getValue("fud");  // "bleh"
	REQUIRE(val2 == "bleh");
	cli::strlist rest = opt.getRest();
	// {"fizz","buzz"}
	REQUIRE(rest.size() == 2);
	REQUIRE(rest[0] == "fizz");
	REQUIRE(rest[1] == "buzz");

	REQUIRE( 2 == 2);
}

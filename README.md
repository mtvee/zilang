# Zi Script

Zi is a small, interpreted scheme-like language intended for embedding into a c++ program to facilitate scripting. I am using it for a game I am building but thought it might be of some use to others.

## What it is

- basic parenthetical expression evaluator
- uses std::shared\_ptr for everything so no GC required
- context (modules) for name space like uses
- written in c++14 so it's super easy to embed

## What it is not

- not scheme or lisp ()
- non-hygeneic macros
- no garbage collector (uses std::shared\_ptr for everything)
- uses std::exception for errors so it is possible to leak memory in certain cases (workin on this)
- no call/cc

## Building
```bash
    cd $PROJECT_LOCATION
    mkdir build
    cd build
    cmake -G "whatever your generator is" ..
```

## Installing

Make a directory called `.zi` in your home directory and copy `scripts/zirc` into it. Then you should be able to run the example scripts.

```bash
    cd $PROJECT_LOCATION
    mkdir $HOME/.zi
    cp scripts/zirc $HOME/.zi/zirc
```


## Embedding

Embedding is a matter of including `zi.cpp` and `zi.h` into your build. Then in your c++ code:
```cpp
    #include <iostream.h>
    #include "zi.h"

    int main( int argc, char argv )
    {
        zi::init();
        zi::snode ret = zi::eval_string("(+ 1 1)");
        std::cout << "1+1=" << ret.as_string() << std::endl;
        return 0;
    }
```

### Setting variables
```cpp
    zi::set("varname", data )
```

`data` can be basically any of the standard c/c++ types as well as function
pointers, lambda functions, object pointers and pointers to object member
data and functions.

Some examples:
```cpp
    zi::set("one", 1);
    zi::set("PI", 3.1415926);

	// create a func with a c++ lambda
    // note: the following does no error checking for arguments
    zi::set("adder",
		static_cast<zi::builtin>([](zi::snodelist &args, zi::senvironment &env) {
		    return zi::make_snode(args[0]->to_double() + args[1]->to_double());
                }));

    zi::snode ret = zi.eval_string("(adder PI 1)");
    std::cout << ret->to_int() << std::endl;
```
### Executing scripts

```cpp
    std::string code;
    zi::slurp("somefile.zil", code);
    zi::eval_string(code);
```

### Catching errors

zi throws `zi::zi_exception()` on any error so you just need to 
catch it in your c++ code.

It is currently possible to leak memory if exceptions are thrown around. I am working on this.

There is more detailed info in [embedding.md](docs/embedding.md)

## The Language

Zi is a functional language in the vein of lisp/scheme but makes
no attempt to be compatable with either. It does however utilize the
familiar `s-expressions`, parenthesized lists and prefix operators of
the lisp/scheme dialects and so should feel relatively comfortable if
you have experience with any of those.

For example, here is an zi snippet to calculate fibonacci numbers:

```scheme
    (defn fib (n)
      (if (<= n 2)
          1
          (+ (fib (- n 1)) (fib (- n 2)))))
    (println (fib 10))
```

Please see [zilang.md](docs/zilang.md) for the full language documentation.

## License

Zi is licensed under the [APACHE License, Version 2.0][2].

## Contributing

- Check for open issues or open a fresh issue to start a discussion around a feature idea or a bug.
- Fork the [repository][3] to start making your changes to the master branch (or branch off of it).
- Write a test which shows that the bug was fixed or that the feature works as expected.
- Send a pull request to bug the maintainer until it gets merged and published. :) Make sure to add yourself to [AUTHORS.md](AUTHORS.md).

## Thanks

Zi was built and expands on [paren][1] so a big thank you to Kim, Taegyoon for creating and making that available.


[1]: https://bitbucket.org/ktg/paren "Paren @BitBucket"
[2]: https://opensource.org/licenses/Apache-2.0 "Apache Licence"
[3]: https://bitbucket.org/mtvee/zilang "ZiLang @BitBucket"
# Zi Language

Zi is a functional language in the vein of lisp/scheme but makes no attempt to be compatable with either. It does however utilize the familiar `s-expressions`, parenthesized lists and prefix operators of the lisp/scheme dialects and so should feel relatively comfortable if you have that knowledge.

At the moment this language is used as a scripting language in a game environment to script NPC ai. The focus is primarily on that but it is becoming more generally useful as it evolves.

## Features

- easy embedding!! :+1
- internally uses shared pointers so cleanup is a snap (no gc)
- most _standard_ scheme constructs
- non-hygeneic macros
- named let
- contexts (isolated environments)

## Types

- bool
- int
- double
- string
- file handle
- user pointers

## Core Functions

**\+**

_function_

```scheme
(+ x...)
```
Returns the sum of nums.

----

**\-**

_function_

```scheme
(- x...)
```

If no ys are supplied, returns the negation of x, else subtracts
the ys from x and returns the result.

----

**/**

_special form_

```scheme
(/ x...)
```

----

**\***

_function_

```scheme
(* x...)
```

Returns the product of nums.

----

**&lt;**

_special form_

```scheme
(< x y)
```

Returns non-nil if nums are in monotonically increasing order,
otherwise false.

----


**and**

_special form_

```scheme
(and)
(and x)
(and x y...)
```

Evaluates exprs one at a time, from left to right. If a form returns logical false (nil or false), and returns that value and doesn't evaluate any of the other expressions, otherwise it returns the value of the last expr. `(and)` returns true.

----

**append**

_function_

```scheme
(append x list)
```

adds x to list and returns list

----

**apply**

_function_

```scheme
(apply f x)
(apply f x y ...)
```

Applies fn f to the argument list formed by prepending intervening arguments to args.

----

**atom?**

_function_

```scheme
(atom? x)
```

Returns true is x is an atom

----

**begin**

_special form_

```scheme
(begin
  block)
```

The blocks are evaluated sequentially from left to right, and the value of the last block is returned

----

**car**

_function_

```scheme
(car list)
```

Returns the first item in list

----

**ceil**

_function_

```scheme
(ceil x)
```

Returns the smallest integer not smaller than x

----

**cdr**

_function_

```scheme
(cdr list)
```

Returns list without the first item

----

**chr**

_function_

```scheme
(chr x)
```

Returns character of ascii code x

----

**cond**

_special form_

```scheme
(cond
   (clauses)
   ...)
```

Takes a set of test/expr pairs. It evaluates each test one at a time.  If a test returns logical true, cond evaluates and returns the value of the corresponding expr and doesn't evaluate any of the other tests or exprs. (cond) returns nil.

----

**cons**

_function_

```scheme
(cons x seq)
```
Returns a new seq where x is the first element and seq is
the rest.

----

**context**

_function_

```scheme
(context x?)
```

Returns the name of the current context. If given x, creates a new context called x and switches to it.

----

**current-time**

_function_

```scheme
(current-time)
```

Returns time in seconds since the epoch.

----

**dec**

_function_
```scheme
(dec x)
```
Returns a number one less than num. 

----

**def**

_special form_
```scheme
(def symbol init?)
```
Creates and interns a global var with the name of symbol in the current namespace (*ns*) or locates such a var if it already exists.  If init is supplied, it is evaluated, and the root binding of the var is set to the resulting value.  If init is not supplied, the root binding of the var is unaffected.

----

**defmacro**

_function_
```scheme
(defmacro name doc-string? attr-map? [params*] body)
(defmacro name doc-string? attr-map? ([params*] body) + attr-map?)
```
Like def, but the resulting function name is declared as a macro and will be used as a macro by the compiler when it is called.

----

**double**

_function_

```scheme
(double x)
```

Coerces x into a double

----

**dump**

_function_

```scheme
(dump)
```

Prints summmary of the current context to stdout

----

**eval**

_function_
```scheme
(eval form)
```
Evaluates the form data structure (not text!) and returns the result.

----

**exists?**

_function_

```scheme
(exists? x)
```

Returns true if x is defined in any way

----

**expt**

_function_

```scheme
(expt x y)
```

Returns x raised to the power y

----

**filter**

_function_
```scheme
(filter pred)
(filter pred coll)
```
Returns a lazy sequence of the items in coll for which(pred item) returns true. pred must be free of side-effects. Returns a transducer when no collection is provided.

----

**floor**

_function_

```scheme
(floor x)
```
Return the largest integer value not greater than x
----

**fn**

_special form_
```scheme
(fn name? (params*) exprs*)
(fn name? (params*) (exprs* ...)
```

Defines a function

----

**fold**

_function_

```scheme
(fold x y)
```

----

**if**

_special form_

```scheme
(if test
  then
  else?)
```
Evaluates test. If not the singular values nil or false,
evaluates and yields then, otherwise, evaluates and yields else. If else is not supplied it defaults to nil.

----

**inc**

_function_
```scheme
(inc x)
```
Returns a number one greater than num.

----

**int**

_function_
```scheme
(int x)
```
Coerce to int

----

**length**

_function_

```scheme
(length x)
```
Return the length of a list or characters in a string.

----

**let**

_special form_
```scheme
(let name? [bindings ...] exprs ...)
```
Evaluates the exprs in a lexical context in which the symbols in
the binding-forms are bound to their respective init-exprs or parts
therein.

----

**list**

_function_
```scheme
(list items...)
```
Creates a new list containing the items.

----

**ln**

_function_

```scheme
(ln x)
```
Returns the the natural (base e) logarithm of x

----

**load**

_function_

```scheme
(load x)
```

Load and eval a file. Context always returns to 'MAIN after load.

----

**log10**

_function_

```scheme
(log10 x)
```

Returns the common (base-10) logarithm of arg

----

**map**

_function_
```scheme
(map f)
(map f coll)
(map f c1 c2)
(map f c1 c2 c3)
(map f c1 c2 c3 & colls)
```
Returns a lazy sequence consisting of the result of applying f to the set of first items of each coll, followed by applying f to the set of second items in each coll, until any one of the colls is exhausted.  Any remaining items in other colls are ignored. Function f should accept number-of-colls arguments. Returns a transducer when no collection is provided.

----

**max**

_function_

```scheme
(max x y...)
(max list)
```

Return the highest value in args or list

----

**min**

_function_

```scheme
(min x y...)
(min list)
```

Returns the lowest value in args or list

----

**modulo**

_function_

```scheme
(modulo x y)
```
Modulus of x and y. Truncates toward negative infinity.

----

**not**

_function_

```scheme
(not x)
```

not returns t if x is nil, and otherwise returns nil

----

**nth**

_function_

```scheme
(nth x list)
```

Returns the x indexed item from list. Zero based

----

**null?**

_function_

```scheme
(null? x)
```

Returns true if x is nil

----

**or**

_special form_

```scheme
(or x...)
```

Evaluates each form, one at a time, from left to right. If any form other than the last evaluates to something other than nil, or immediately returns that non-nil value without evaluating the remaining forms. If every form but the last evaluates to nil, or returns whatever evaluation of the last of the forms returns. Therefore in general or can be used both for logical operations, where nil stands for false and non-nil values stand for true, and as a conditional expression.
 
----

**pop-back!**

_function_

```scheme
(pop-back! list)
```
Removes the last item from list and returns it.

----

**print**

_function_

```scheme
(print stream? x...)
```

Print x to the stream. Default stream is stdout

----

**println**

_function_

```scheme
(println stream? x...)
```

Print x to the stream followed by a newline. Default stream is stdout

----

**push-back!**

_function_

```scheme
(push-back! x list)
```

Pushes x to the end of list

----

**quote**

_special form_

```scheme
(quote x)
```

Prevents evaluation of x

----

**rand**

_function_

```scheme
(rand)
```
Returns a pseudo-random integral value between ​0​ and RAND_MAX 

----

**set**

_special form_

```scheme
(set name x)
```

Same as def

----

**sin cos tan asin acos atan**

_function_

```scheme
(sin x)
```

Returns the triganometric value of x

----

**sqrt**

_function_

```scheme
(sqrt x)
```

Returns the square root of x

----

**string**

_function_

```scheme
(string x)
```

Coerce x to a string

----

**substring**

_function_

```scheme
(substr string x y?)
```

Return the sub string of string starting at x for y chars. Y defaults to the end of the string.

----

**type**

_function_

```scheme
(type x)
```

Returns the type of x

----

**while**

_special form_

```scheme
(while cond block)
```

Loops through block, checking cond is true. The loop stops when condition returns false or nil

----

**true**

_function_

```scheme
true
```

True is not false

----

**false**

_function_

```scheme
false
```
False is not true or nil

----

## Variables

**E**

_variable_

```scheme
E
```
Euler's constant (2.71828182845904523536)

----

**PI**

_variable_

```scheme
PI
```

Delicious pi (3.14159265358979323846)

----

**zi.build**

_variable_

```scheme
zi.build```

A string representing the Zi build

----

**zi.library**

_variable_

```scheme
zi.library```

System path to the library

----

**zi.version**

_variable_

```scheme
zi.version
```

The Zi version as a string.

----

## IO

**close**

_function_

```scheme
(close x)
```

Closes the stream x. Returns true if sucessful

----

**dump**

_function_

```scheme
(dump)
```

Prints a summary of the current context to stdout

----

**exit**

_function_

```scheme
(exit x?)
```

Aborts and returns to the OS

----

**getenv**

_function_

```scheme
(getenv x)
```

Returns environment variable x

----

**open**

_function_

```scheme
(open x)
```

Open x file and return a stream atom or false

----

**read-file**

_function_

```scheme
(read-file x)
```

Read file x and return the contents as a string

----

**read-string**

_function_

```scheme
(read-string)
```

Read a string from sdtin

----

**system**

_function_

```scheme
(system x...)
```

Calls the system with command x

----

**write-file**

_function_

```scheme
(write-file x y)
```

Write string y to file stream x

----

## REPL Commands

**.help**

Print a help line in the repl

**.exit**

Terminate the repl and return to the OS







# Embedding Zi in C++

Embedding is simply a matter of including `zi.cpp` and `zi.h` into your
build. Then in your c++ code:

    #include <iostream.h>
    #include "zi.h"

    int main( int argc, char argv )
    {
        zi::init();
        zi::snode ret = zi::eval_string("(+ 1 1)");
        std::cout << "1+1=" << ret.as_string() << std::endl;
        return 0;
    }

### Setting variables

    zi::set("varname", data )

`data` can be basically any of the standard c/c++ types as well as function
pointers, lambda functions, object pointers and pointers to object member
data and functions.

Some examples:

    zi::set("one", 1);
    zi::set("PI", 3.1415926);

    // note: the following does no error checking for arguments
    zi::set("adder", 
		static_cast<zi::builtin>([](zi::snodelist &args, zi::senvironment &env) {
		    return zi::make_snode(args[0]->to_int() + args[1]->to_int()); 
                }));

    zi::snode ret = zi.eval("(adder PI 1)");
    std::cout << ret->to_int() << std::endl;

### Executing scripts

    std::string code;
    zi::slurp("somefile.zil", code);
    zi::eval_string(code);

### Catching errors

zi throws `zi::zi_exception()` on any error so you just need to 
catch it in your c++ code.


## TODO

Add more examples
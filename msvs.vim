" Vim compiler file
" Compiler:	Visual Stewdio

"if exists("current_compiler")
"  finish
"endif
let current_compiler = "msvs"

setlocal errorformat=\ %#%f(%l)\ :\ %#%t%[A-z]%#\ %m
setlocal makeprg=devenv.com\ ALL_BUILD.vcxproj\ /Build
